#!/bin/bash

CONFIG_FILE=/rso/config/serverConfig.xml
CONFIG_SERVER_FILE=./build/server/serverConfig.xml
DATABASE_IP=/rso/config/ip.txt

if [ ! -f $CONFIG_FILE ]; then
    echo "File not found! Add serverConfig.xml to /tmp/rso_config/ on your host"
    exit
fi

if [ ! -f $DATABASE_IP ]; then
    echo "File not found! Add ip.txt to /tmp/rso_config/ on your host"
    exit
fi

cp $CONFIG_FILE $CONFIG_SERVER_FILE

cd ./build/server/
cp $DATABASE_IP .
./server -m





