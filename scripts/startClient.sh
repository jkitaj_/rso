#!/bin/bash

CONFIG_FILE=/rso/config/clientConfig.xml
CONFIG_CLIENT_FILE=./build/client/clientConfig.xml

if [ ! -f $CONFIG_FILE ]; then
    echo "File not found! Add clientConfig.xml to /tmp/rso_config/ on your host"
    exit
fi

cp $CONFIG_FILE $CONFIG_CLIENT_FILE

cd ./build/client/
./client_gui




