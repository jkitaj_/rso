#!/bin/bash

CONFIG_FILE=/rso/config/nodeConfig.xml
CONFIG_NODE_FILE=./build/node/nodeConfig.xml

if [ ! -f $CONFIG_FILE ]; then
    echo "File not found! Add nodeConfig.xml to /tmp/rso_config/ on your host"
    exit
fi

cp $CONFIG_FILE $CONFIG_NODE_FILE

cd ./build/node
./node

