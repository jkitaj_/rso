#!/bin/bash

CONFIG_FILE_TO_EDIT=/rso/config/cassandra.yaml
BASE_CASSANDRA=/cassandra/apache-cassandra-2.1.5
CONFIG_FILE_ON_CASSANDRA=$BASE_CASSANDRA/conf/cassandra.yaml

if [ ! -f $CONFIG_FILE_TO_EDIT ]; then
    echo "File not found! Add cassandra.yaml to /tmp/rso_config/ on your host"
    exit
fi

cp $CONFIG_FILE_TO_EDIT $CONFIG_FILE_ON_CASSANDRA

$BASE_CASSANDRA/bin/cassandra -f





