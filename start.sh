#!/bin/bash

while true
do
    case "$1" in
        -c|--client)
            sudo docker run --name="client" --rm=true --net="host" -v /tmp/rso_config:/rso/config -it rsoweiti/rso sh ./scripts/startClient.sh
            exit 1
            ;;
        -s|--server)
            sudo docker run --name="server" --rm=true --net="host" -v /tmp/rso_config:/rso/config -it rsoweiti/rso sh ./scripts/startServer.sh
            exit 1
            ;;
        -n|--node)
            sudo docker run --name="node" --rm=true --net="host" -v /tmp/rso_config:/rso/config -it rsoweiti/rso sh ./scripts/startNode.sh
            exit 1
            ;;
	-d|--database)
            sudo docker run --name="db" --rm=true --net="host" -v /tmp/rso_config:/rso/config -it rsoweiti/rso sh ./scripts/startDB.sh
            exit 1
            ;;
        *)
            echo "Use -c or --client to start client, -s or --server to start server, -n or --node to start node, -d or --database to database node" >&2
            exit 1
            ;;
    esac
done




