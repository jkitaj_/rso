#ifndef RSO_NODECOMMUNICATIONMODULE_H
#define RSO_NODECOMMUNICATIONMODULE_H

#include <QObject>
#include <thread>
#include <mutex>
#include <Credentials.h>
#include "CommunicationModule.h"
#include "node.h"

class MessageRound;

class NodeCommunicationModule : public CommunicationModule {
    Q_OBJECT

public:
    NodeCommunicationModule(const Credentials&credentials_, Node *node_, MessageRound *round, const std::string& configFileName, QObject *parent = 0);
    virtual ~NodeCommunicationModule();
    const std::string SERVER_SECTION = "server";
    void sendTasksBackToServer(MessageRound *msgQueue);
    Host getMyInterfaceFromConfiguration();
    void start();

protected:
    virtual void onReceive(Message& message);
    virtual void onFailedToSendMessage(const Host& receiver, const Message& message);

private:
    Node *node;
    MessageRound *round;
    Credentials credentials;
    std::mutex accessConfig;

    // 3 methods below require lock on accessConfig
    void forgetAllServers();
    void addServer(const Host& server);
    std::list<Host> getServers();

    void sendBackThreadFunction(MessageRound *queue);
    Host getRandomServer(const std::list<Host>& servers);

    std::thread *sendBackThread;
    volatile bool run = false;
};

#endif //RSO_NODECOMMUNICATIONMODULE_H
