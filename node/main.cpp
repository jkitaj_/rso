#include <QtCore/qcoreapplication.h>
#include <networkConfig.h>
#include "NodeCommunicationModule.h"
#include "node.h"
#include "MessageRound.h"
#include <QtCore/qcommandlineparser.h>
#include <CredentialsReader.h>

int main(int argc, char *argv[]) {
    QCoreApplication app(argc, argv);

	QCoreApplication::setApplicationName("rso_node");
	QCoreApplication::setApplicationVersion("0.0.1");

	QCommandLineParser parser;
	parser.setApplicationDescription("This is node");
	parser.addHelpOption();
	parser.addVersionOption();
	
	CredentialsReader credentialsReader;
	credentialsReader.initialize(parser);
	parser.process(app);
	Credentials credentials = credentialsReader.read(parser, app);

	Node wezel;
	MessageRound Round;

	wezel.setMessageRound(&Round);

	NodeCommunicationModule module(credentials, &wezel, &Round, NODE_CONFIG_FILE_NAME);
	module.listenToConnectionsFrom(module.SERVER_SECTION);
	module.start();
	//module.sendTasksBackToServer(&Round);	// starts reading from Round and sending back to server

	std::string address;
	try {
		address = module.getMyInterfaceFromConfiguration().getAddress();
	} catch(const std::invalid_argument& e) {
		qWarning() << "main: " << e.what();
		return 1;
	}
	wezel.setAddress(address);
	return app.exec();
}
