#ifndef RSO_NODE_H
#define RSO_NODE_H

#include <thread>
#include <chrono>
#include <iostream>
#include <string>
#include "database.h"
#include "MessageRound.h"
#include <Message.h>
#include "Runnable.h"


/*class Runnable{
public:
    void start(){
        if(run_==false){
            run_=true;
            t = std::thread(&Runnable::run, this);
        }else{
            std::cout<<"Object already is runing"<<std::endl;
        }
    }
    void stop(){run_=false; };
    explicit Runnable(){ run_=false;	}
    ~Runnable() {};
protected:
    virtual void run()=0;
    bool run_;
private:
    std::thread t;
};*/

class Node: public Runnable{
	int i;
	Message message;
	MessageRound* round;
	std::string address;

public:
    Node()
            : Runnable(),
              message() {}
    ~Node() {};
    void setMessageRound(MessageRound* rd);
    void setAddress(std::string a){address=a;}
protected:
    void run();
    void selectTask(Message& mes);
    void doTask(Message& mes);
};

#endif // RSO_NODE_H
