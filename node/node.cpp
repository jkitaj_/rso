#include "node.h"
#include "database.h"

void Node::run(){
	while(run_){
		selectTask(message);
		doTask(message);
		std::this_thread::sleep_for(std::chrono::milliseconds(3000));
	}
}

void Node::selectTask(Message& mes){
	DataBase* base= new DataBase();
   
	bool var=false;
	if(base!=NULL){
		while(var==false){
			std::cout<<"Trying to select overdone task"<<std::endl;
			var=base->select_task_to_work(mes, Message::State::INPROGRES);
			if(var==false){
				std::cout<<"There is no overdone task in database"<<std::endl;
				std::cout<<"Trying to select new added task"<<std::endl;
				var=base->select_task_to_work(mes, Message::State::INQUEUE);
				std::cout<<"Trying sucseed"<<std::endl;
			}
			if(var==false){
				std::cout<<"New task not found, going to sleep"<<std::endl;
				std::this_thread::sleep_for(std::chrono::milliseconds(3000));
			}else{
				std::cout<<" Task found, trying to update task state and timestam"<<std::endl;
				/*Dodać update statusu na INPROGRES*/
				//var=base->select_task_to_work(mes, Message::State::INPROGRES);
				if(mes.getInfo()==std::string("#####")){
					mes.setState(Message::State::ERROR);
					mes.setInfo("2manyOver");
					var=base->update_task(mes);
					continue;
					
				}else{
					mes.setState(Message::State::INPROGRES);
					mes.setInfo(mes.getInfo()+"#");
					mes.setNodeIP(address);
					var=base->update_task(mes);
					if(var==true){
						std::cout<<"Updating complited proceding forward with :"<<std::endl;
						mes.print();
					}else{
						std::cout<<"Updating failed restarting..."<<std::endl;
					}
				}
			}
		}
		base->checkHost(message);
		delete base;
	}else{
		std::cout<<"DataBase Unreacheable"<<std::endl;
	}
}

void Node::doTask(Message& mes){

	DataBase base;
	DataSet data;
	const char* que;

	std::string task=mes.getQuery().c_str();

	if(task=="Test"){
		std::cout<< "Test OK"<<std::endl;
		mes.setResponse("Test OK");
		mes.setState(Message::State::READY);
	}else if(task=="Zadanie1"){
		std::cout<< "Working... "<<std::endl;
		que = "SELECT * FROM mykeyspace.taxpayer;";
		base.select_from_person(que, data);
		mes.setResponse(data.getAnalyse(1));
		mes.setState(Message::State::READY);
	}else if(task=="Zadanie2"){
		std::cout<< "Working... "<<std::endl;
		que = "SELECT * FROM mykeyspace.taxpayer;";
		base.select_from_person(que, data);
		mes.setResponse(data.getAnalyse(2));
		mes.setState(Message::State::READY);
	}else if(task=="Zadanie3"){
		std::cout<< "Working... "<<std::endl;
		que = "SELECT * FROM mykeyspace.taxpayer;";
		base.select_from_person(que, data);
		mes.setResponse(data.getAnalyse(3));
		mes.setState(Message::State::READY);
	}else{
		mes.setState(Message::State::ERROR);
		mes.setResponse("Wrong Function");
		std::cout<< "No such function..."<<std::endl;
	}
	//base.update_task(mes);
	
	round->setMessage(mes);
}

void Node::setMessageRound(MessageRound* rd){
	this->round=rd;
	
}
