#include "NodeCommunicationModule.h"
#include <iostream>
#include "MessageRound.h"
#include "database.h"
#include <QCoreApplication>
#include <QtCore/qfile.h>
#include "node.h"

NodeCommunicationModule::NodeCommunicationModule(
        const Credentials& credentials_,
        Node *node_,
        MessageRound *round_,
        const std::string& configFileName, QObject *parent)
        : CommunicationModule(configFileName, parent),
          sendBackThread(nullptr), node(node_), round(round_), credentials(credentials_)
{
    std::srand(std::time(0));
}

NodeCommunicationModule::~NodeCommunicationModule() {
    if(sendBackThread != nullptr) {
        run = false;
        sendBackThread->join();
        delete sendBackThread;
    }
}

void NodeCommunicationModule::start() {
    Message msg(Message::NODE_GETDBADDRESS);
    Host local;
    try {
        local = getMyInterfaceFromConfiguration();
    } catch(const std::invalid_argument& e) {
        qWarning() << "NodeCommunicationModule: " << e.what();
        exit(1);
    }
    msg.setSender(local);
    msg.setLogin(credentials.getLogin());
    msg.setPassword(credentials.getPassword());

    std::list<Host> servers = getServers();
    if(servers.empty()) {
        qWarning() << "NodeCommunicationModule: ERROR - empty server list in configuration";
        exit(1);    // TODO: clean shutdown
    }
    Host server = servers.front();
    send(server, msg);
}

void NodeCommunicationModule::onReceive(Message& message) {
    if(message.getType() == Message::SERVER_NODERESPDBADDRESS) {
        stopListening();

        std::string dbAddresses = message.getInfo();
        qDebug() << "NodeCommunicationModule: Received database addresses:" << QString::fromStdString(dbAddresses);
        QString fileName = "ip.txt";
        QFile file(fileName);
        if(!file.open(QIODevice::ReadWrite | QIODevice::Truncate | QIODevice::Text)) {
            qWarning() << "NodeCommunicationModule: Failed to create/open file: " << fileName;
            return;
        }
        file.write(dbAddresses.c_str());

        node->start();
        sendTasksBackToServer(round);
    } else {
        qWarning() << "NodeCommunicationModule: Unexpected message:" << message;
    }
//    std::list<Host> servers = message.getServers();
//    qDebug() << "NodeCommunicationModule: Received new list of " << servers.size() << "servers";
//
//    std::lock_guard<std::mutex> guard(accessConfig);
//
//    forgetAllServers();
//    for(Host h: servers) {
//        addServer(h);
//    }
}

std::list<Host> NodeCommunicationModule::getServers() {
    return getConfigurationManager()->getHostsFromSection(SERVER_SECTION);
}

void NodeCommunicationModule::sendTasksBackToServer(MessageRound *msgQueue) {
    if(sendBackThread == nullptr) {
        run = true;
        sendBackThread = new std::thread(&NodeCommunicationModule::sendBackThreadFunction, this, msgQueue);
    } else {
        qWarning() << "NodeCommunicationModule: Thread is already running!";
    }
}

// method running in another thread
void NodeCommunicationModule::sendBackThreadFunction(MessageRound *queue) {
    while(run) {
        Message msg;
        queue->getMessage(msg);
        msg.setSenderIP(msg.getNodeIP());
        msg.setSenderPort(6666);
        Host orygineServer;
        orygineServer.setAddress(msg.getServerIP());
        orygineServer.setPort(6666);
		
        send(orygineServer, msg);    // send should be thread-safe
    }
}

// this should be called in Qt (main) thread
void NodeCommunicationModule::onFailedToSendMessage(const Host &receiver, const Message &message) {
    Message msg = message;

    if(msg.getType() == Message::NODE_GETDBADDRESS) {
        qWarning() << "NodeCommunicationModule: Cannot contact server";
    } else {
        qDebug() << "NodeCommunicationModule: Failed to send message" << message << "to server" << receiver;

        msg.addServer(receiver);    // save faulty server in message - some other server should check it
        std::list<Host> faultyServers = msg.getServers();

        std::list<Host> knownServers;
        DataBase base;
        base.getServerList(knownServers);// = getServers();

        for(Host& fs : faultyServers) // remove servers which are faulty - we already tried to send to them
            knownServers.remove(fs);

        if(knownServers.empty()) {
            qWarning() << "NodeCommunicationModule: Cannot send message" << message
            << "tried to send to all known servers but failed!";
        } else {
            Host randomServer = getRandomServer(knownServers);  // maybe this server is ok
            send(randomServer, msg); // send can also be called from "send back thread" but it should be ok
        }
    }
}

void NodeCommunicationModule::forgetAllServers() {
    getConfigurationManager()->removeAllHostsFromSection(SERVER_SECTION);
}

void NodeCommunicationModule::addServer(const Host &server) {
    getConfigurationManager()->addHostToSection(server, SERVER_SECTION);
}

Host NodeCommunicationModule::getRandomServer(const std::list<Host> &servers) {
    int idx = static_cast<int>(std::rand() % servers.size());
    return *std::next(servers.begin(), idx);
}

Host NodeCommunicationModule::getMyInterfaceFromConfiguration() {
    return getConfigurationManager()->getCurrentConfiguration().getSectionByName(SERVER_SECTION).listen;
}
