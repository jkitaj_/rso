#ifndef RSO_CREDENTIALSREADER_H
#define RSO_CREDENTIALSREADER_H

#include <QtCore/qcoreapplication.h>
#include <QtCore/qcommandlineoption.h>
#include "Credentials.h"

class QCommandLineParser;

class CredentialsReader {
public:
    CredentialsReader();
    void initialize(QCommandLineParser& parser);
    Credentials read(QCommandLineParser &parser, const QCoreApplication& app);

private:
    const QString notProvided;
    QCommandLineOption loginOption;
    QCommandLineOption passwordOption;
};

#endif //RSO_CREDENTIALSREADER_H
