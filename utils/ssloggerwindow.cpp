#include <QtWidgets/qboxlayout.h>
#include "ssloggerwindow.h"

#include <QPushButton>

SSLoggerWindow::SSLoggerWindow(QWidget *parent) :
        CMLoggerWindow(parent)
{
    Q_INIT_RESOURCE(qtres);

    setWindowTitle("Logger: server to server");
    roleLabel = new QLabel(this);
    roleLabel->setFixedHeight(65);
    CMLoggerWindow::getMainLayout()->insertWidget(0, roleLabel);
}

SSLoggerWindow::~SSLoggerWindow()
{
}

#include <iostream>

void SSLoggerWindow::appednMessage(Mode mode, const Message &message, const Host &host, const ServerListenModule::Role &role)
{
    setRole(role);
    CMLoggerWindow::appednMessage(mode, message, host);
}

void SSLoggerWindow::setMessageInfo(const std::string &info, const ServerListenModule::Role &role)
{
    setRole(role);
    CMLoggerWindow::setMessageInfo(info);
}

void SSLoggerWindow::setRole(const ServerListenModule::Role role)
{
    QPixmap pixmap;
    switch (role) {
        case ServerListenModule::Role::LEADER:
            pixmap.load(":/images/leader.png");
            break;
        case ServerListenModule::Role::SLAVE:
            pixmap.load(":/images/slave.png");
            break;
        case ServerListenModule::Role::NEW:
            pixmap.load(":/images/new.png");
            break;
        case ServerListenModule::Role::NEW_AUTHENTICATED:
            pixmap.load(":/images/new_authenticated.png");
            break;
        case ServerListenModule::Role::NEW_NOAUTHENTICATED:
            pixmap.load(":/images/new_noauthenticated.png");
            break;
        case ServerListenModule::Role::CLEADER:
            pixmap.load(":/images/cleader.png");
            break;
    }
    roleLabel->setPixmap(pixmap);
}