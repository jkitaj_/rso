#include "CredentialsReader.h"
#include <QCommandLineParser>
#include <iostream>
// TODO:
#define prompt std::cout<<"server> "

CredentialsReader::CredentialsReader() : notProvided("not-provided"),
    loginOption(QCommandLineOption(QStringList() << "l" << "login",
                                 "Login to access database: <login>.", "login", notProvided)),
    passwordOption(QCommandLineOption(QStringList() << "p" << "password",
                                    "Password to access database: <password>.", "password", notProvided)) {
}

Credentials CredentialsReader::read(QCommandLineParser &parser, const QCoreApplication &app) {
    std::string login = parser.value(loginOption).toStdString();
    std::string password = parser.value(passwordOption).toStdString();

    std::cout << "==============================" << std::endl;
    prompt << "Credentials to access database:" << std::endl;
    prompt << "Login: ";
    if(login == notProvided.toStdString()) {
        std::cin >> login;
    } else {
        std::cout << login << std::endl;
    }
    prompt << "Password: ";
    if(password == notProvided.toStdString()) {
        std::cin >> password;
    } else {
        std::cout << password << std::endl;
    }
    std::cout << "==============================" << std::endl << std::endl;
    return Credentials(login, password);
}

void CredentialsReader::initialize(QCommandLineParser &parser) {
    QCommandLineOption loginOption = QCommandLineOption(QStringList() << "l" << "login",
                                   "Login to access database: <login>.", "login", notProvided);
    QCommandLineOption passwordOption = QCommandLineOption(QStringList() << "p" << "password",
                                      "Password to access database: <password>.", "password", notProvided);
    parser.addOption(loginOption);
    parser.addOption(passwordOption);
}

