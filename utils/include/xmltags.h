#ifndef XMLTAGS_H
#define XMLTAGS_H

class XMLTag
{
public:
    const constexpr static char* mainTag = "RSO_system";
    const constexpr static char* typeTag = "type";

    struct TypeAttrib
    {
        const constexpr static char* typeAttrib = "type";
    };

    const constexpr static char* textTag = "text";

    struct TextAttrib
    {
        const constexpr static char* textAttrib = "text";
    };

    const constexpr static char* okTag = "ok";

    struct OkAttrib
    {
        const constexpr static char* okAttrib = "ok";
    };

    // new
    const constexpr static char* idTag = "Id";

    struct IdAttrib
    {
        const constexpr static char* idAttrib = "id";
    };

    const constexpr static char* clientTag = "Client";

    struct ClientAttrib
    {
        const constexpr static char* ipAttrib = "ip";
        const constexpr static char* portAttrib = "port";
    };

    const constexpr static char* serverTag = "Server";

    struct ServerAttrib
    {
        const constexpr static char* ipAttrib = "ip";
        const constexpr static char* portAttrib = "port";
    };

    const constexpr static char* nodeTag = "Node";

    struct NodeAttrib
    {
        const constexpr static char* ipAttrib = "ip";
        const constexpr static char* portAttrib = "port";
    };

    const constexpr static char* senderTag = "Sender";

    struct SenderAttrib
    {
        const constexpr static char* ipAttrib = "ip";
        const constexpr static char* portAttrib = "port";
    };

    const constexpr static char* stateTag = "State";

    struct StateAttrib
    {
        const constexpr static char* stateAttrib = "state";
    };

    const constexpr static char* queryTag = "Query";

    struct QueryAttrib
    {
        const constexpr static char* queryAttrib = "query";
    };

    const constexpr static char* responseTag = "Response";

    struct ResponseAttrib
    {
        const constexpr static char* responseAttrib = "response";
    };

    const constexpr static char* infoTag = "Info";

    struct InfoAttrib
    {
        const constexpr static char* infoAttrib = "info";
    };

    const constexpr static char* credentialsTag = "Credentials";

    struct CredentialsAttrib
    {
        const constexpr static char* loginAttrib = "Login";
        const constexpr static char* passwordAttrib = "Password";
    };

    const constexpr static char* serversTag = "Servers";

    struct ServersAttrib
    {
        const constexpr static char* serversAttrib = "servers";
    };

};

#endif //XMLTAGS_H
