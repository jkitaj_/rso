#ifndef RSO_NETWORKCONFIG_H
#define RSO_NETWORKCONFIG_H

const std::string CLIENT_CONFIG_FILE_NAME = "clientConfig.xml";
const std::string SERVER_CONFIG_FILE_NAME = "serverConfig.xml";
const std::string NODE_CONFIG_FILE_NAME = "nodeConfig.xml";

const uint16_t CLIENT_LISTEN_PORT = 2222;   // port na ktorym klient nasluchuje polaczen od serwera
const uint16_t SERVER_LISTEN_PORT_FOR_CLIENT = 3333;    // port na ktorym serwer nasluchuje polaczen od klienta
const uint16_t SERVER_LISTEN_PORT_FOR_NODE = 6666;  // port na ktorym serwer nasluchuje polaczen od wezla
const uint16_t SERVER_LISTEN_PORT_FOR_SERVER = 5555;  // port na ktorym serwer nasluchuje polaczen od serwera
const uint16_t NODE_LISTEN_PORT = 4444;   // port na ktorym wezel nasluchuje polaczen od serwera

#endif //RSO_NETWORKCONFIG_H
