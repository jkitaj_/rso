#include "cmloggerwindow.h"
#include <iostream>

//Qt
#include <qtablewidget.h>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QPlainTextEdit>
#include <QSplitter>
//#include <QWidget>
//#include <QtWidgets/QApplication>

//Q_INIT_RESOURCE(qtres);

CMLoggerWindow::CMLoggerWindow(QWidget *parent) :
        QMainWindow(parent)
{
    Q_INIT_RESOURCE(qtres);

    QWidget *mainWidget = new QWidget(this);
    setCentralWidget(mainWidget);

    m_mainLayout = new QVBoxLayout(mainWidget);
    QSplitter *messageWidget = new QSplitter(mainWidget);
    {
        m_messageTableWidget = createMessageTable();
        messageWidget->addWidget(m_messageTableWidget);

        m_messageInfoPlainText = new QPlainTextEdit(messageWidget);
        messageWidget->addWidget(m_messageInfoPlainText);
    }
    m_mainLayout->addWidget(messageWidget);

    connect(m_messageTableWidget, SIGNAL(cellClicked(int,int)), this, SLOT(cellClicked(int,int)));
}

CMLoggerWindow::~CMLoggerWindow()
{
}

QTableWidget* CMLoggerWindow::createMessageTable()
{
    QTableWidget *messageTable = new QTableWidget();

    messageTable->setRowCount(0);
    messageTable->setColumnCount(5);

    messageTable->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    messageTable->setHorizontalHeaderLabels(QString("ID;Mode;Type;Sender;Receiver").split(";"));

    return messageTable;
}

void CMLoggerWindow::appednMessage(Mode mode, const Message &message, const Host &receiver)
{
    m_messageTableWidget->insertRow( m_messageTableWidget->rowCount() );
    int rowCount = m_messageTableWidget->rowCount();

    QTableWidgetItem* tableItem = new QTableWidgetItem();

    tableItem->setText(QString::fromStdString(std::to_string(message.getID())));
    m_messageTableWidget->setItem(rowCount-1, 0, tableItem);

    QBrush* brush = new QBrush();
    QPixmap pixmap;

    tableItem = new QTableWidgetItem();
    if (mode == Mode::INCOMING) {
        tableItem->setText("INCOMING");
        pixmap.load(":/images/incoming.png");
    } else if (mode == Mode::OUTGOING) {
        tableItem->setText("OUTGOING");
        pixmap.load(":/images/outgoing.png");
    } else if (mode == Mode::MISSED) {
        tableItem->setText("MISSED");
        pixmap.load(":/images/missed.png");
    }
    tableItem->setData(Qt::DecorationRole, pixmap);
    m_messageTableWidget->setItem(rowCount-1, 1, tableItem);

    tableItem = new QTableWidgetItem();
    tableItem->setText(message.getMessageType());
    m_messageTableWidget->setItem(rowCount-1, 2, tableItem);

    tableItem = new QTableWidgetItem();
    QString sender = QString("%1:%2").arg(QString::fromStdString(message.getSender().getAddress())).arg(message.getSender().getPort());
    tableItem->setText(sender);

    m_messageTableWidget->setItem(rowCount-1, 3, tableItem);

    tableItem = new QTableWidgetItem();
    QString receiver_string = QString("%1:%2").arg(QString::fromStdString(receiver.getAddress())).arg(receiver.getPort());
    tableItem->setText(receiver_string);

    m_messageTableWidget->setItem(rowCount-1, 4, tableItem);

    m_messageTableWidget->resizeRowsToContents();
    m_messageTableWidget->resizeColumnsToContents();

    setMessageInfo("");

    m_messageTableWidget->scrollToBottom();
}

void CMLoggerWindow::setMessageInfo(const std::string &info)
{
    m_messageInfoVector.resize(m_messageTableWidget->rowCount());
    m_messageInfoVector[m_messageTableWidget->rowCount()-1] = info;
    m_messageInfoPlainText->setPlainText(QString::fromStdString(info));
}

void CMLoggerWindow::cellClicked(int row, int column)
{
    m_messageInfoPlainText->setPlainText(QString::fromStdString(m_messageInfoVector[row]));
}