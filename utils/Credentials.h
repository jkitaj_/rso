#ifndef RSO_CREDENTIALS_H
#define RSO_CREDENTIALS_H

#include <string>

class Credentials {
public:
    Credentials(const std::string& login_, const std::string& password_)
            : login(login_), password(password_) { }

    std::string getLogin() const {
        return login;
    }

    std::string getPassword() const {
        return password;
    }

private:
    std::string login;
    std::string password;
};

#endif //RSO_CREDENTIALS_H
