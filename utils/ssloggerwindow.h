#ifndef SSLOGGERWINDOW_H
#define SSLOGGERWINDOW_H

#include "cmloggerwindow.h"
#include "../server/ServerListenModule.h"

#include <QLabel>

class SSLoggerWindow : public CMLoggerWindow
{

public:

    explicit SSLoggerWindow(QWidget *parent = 0);
    ~SSLoggerWindow();

    void appednMessage(Mode mode, const Message &message, const Host& host, const ServerListenModule::Role &role);
    void setMessageInfo(const std::string &info, const ServerListenModule::Role &role);

    void setRole(const ServerListenModule::Role role);

private:
    QLabel *roleLabel;
};

#endif // SSLOGGERWINDOW_H
