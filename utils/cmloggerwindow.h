#ifndef CMLOGGERWINDOW_H
#define CMLOGGERWINDOW_H

#include "QMainWindow"

#include "Message.h"
#include "Host.h"

class QVBoxLayout;

class QTableWidget;
class QPlainTextEdit;
class QSplitter;

class CMLoggerWindow : public QMainWindow
{
    Q_OBJECT

public:

    enum Mode {
        OUTGOING,
        INCOMING,
        MISSED,
    } ;

    explicit CMLoggerWindow(QWidget *parent = 0);
    ~CMLoggerWindow();

    void appednMessage(Mode mode, const Message &message, const Host& host);
    void setMessageInfo(const std::string &info);
    QVBoxLayout* getMainLayout() const { return m_mainLayout; }

private:
    QVBoxLayout *m_mainLayout;

    QTableWidget *m_messageTableWidget;
    QPlainTextEdit *m_messageInfoPlainText;

    QTableWidget* createMessageTable();

    std::vector<std::string> m_messageInfoVector;

private slots:
    void cellClicked(int row, int column);
};

#endif // CMLOGGERWINDOW_H
