#ifndef RSO_CONFIGURATION_H
#define RSO_CONFIGURATION_H

#include <string>
#include <list>
#include <QtCore/qstring.h>
#include <QDebug>
#include "Section.h"

class Configuration {
    friend QDebug operator<<(QDebug qdb, const Configuration &c);
public:
    void addSection(Section section);
    Section getSectionByName(const std::string& name) const;
    bool addHostToSection(const Host& host, const std::string& name);
    bool addHostsToSection(const std::list<Host>& hosts, const std::string& name);
    bool setListenHost(const Host& host, const std::string& name);
    bool removeHostFromSection(const Host& host, const std::string& name);
    bool removeAllHostsFromSection(const std::string& name);
    std::list<Section> getSections() const;

private:
    std::list<Section> sections;
};

QDebug operator<<(QDebug qdb, const Configuration& c);

#endif //RSO_CONFIGURATION_H
