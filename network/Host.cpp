#include "Host.h"

QDebug operator<<(QDebug qdb, const Host &h) {
    qdb << "Host ( address:" << QString::fromStdString(h.getAddress()) << " port:" << h.getPort() << ")";
    return qdb;
}

std::ostream & operator<<(std::ostream &out, const Host &h) {
    out << h.getAddress() << ":" << h.getPort();
    return out;
}