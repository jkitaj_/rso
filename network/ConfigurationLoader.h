#ifndef RSO_CONFIGURATIONLOADER_H
#define RSO_CONFIGURATIONLOADER_H

#include <QtCore/qxmlstream.h>

class Configuration;
class Section;
class Host;

class ConfigurationLoader {
public:
    bool loadConfiguration(const QString& fileName, Configuration& config);

private:
    QXmlStreamReader xml;

    bool read(QIODevice *device, Configuration &config);
    void readSections(Configuration &configuration);
    void readSection(Configuration &configuration);
    void readHosts(Section& section);
    void readHost(Host& host);
    QString errorString() const;

    void checkIPAddress(const QString&addressString);
};

#endif //RSO_CONFIGURATIONLOADER_H
