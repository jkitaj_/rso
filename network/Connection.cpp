#include "Connection.h"
#include <QTcpSocket>
#include "Message.h"
#include "MessageConverter.h"

long Connection::lastId = 0;
const QDataStream::Version dataStreamVersion = QDataStream::Qt_5_0;

Connection::Connection(QObject *parent) :
        QObject(parent), id(++lastId), nextBlockSize(0), converter(new MessageConverter())
{
    socket = new QTcpSocket(this);  // socket not yet connected
    initializeSocket();
}

Connection::Connection(QTcpSocket *socket, QObject *parent) :
        QObject(parent), id(++lastId), nextBlockSize(0), converter(new MessageConverter())
{
    this->socket = socket;  // socket should already be connected
    this->socket->setParent(this);
    initializeSocket();
    updateAddresses();
}

Connection::~Connection() {
    delete converter;
}

qint64 Connection::sendData(const QByteArray &message) {
    QByteArray data;
    QDataStream out(&data, QIODevice::WriteOnly);
    out.setVersion(dataStreamVersion);
    out << MessageHeader(0);

    out << message;
    out.device()->seek(0);
    out << MessageHeader(data.size() - sizeof(MessageHeader));

    return socket->write(data); // Returns the number of bytes that were written, or -1 if an error occurred
}

void Connection::onError(QAbstractSocket::SocketError) {
    qWarning() << "Connection error:" << socket->errorString();
}

void Connection::onReadyRead() {
    QDataStream in(socket);

    in.setVersion(dataStreamVersion);
    while(true) {
        if(nextBlockSize == 0) {
            if(quint64(socket->bytesAvailable()) < sizeof(MessageHeader))
                return;
            in >> nextBlockSize;
        }
        if(socket->bytesAvailable() < nextBlockSize)
            return;

        QByteArray xml;
        in >> xml;
        Message message = converter->fromXmlString(QString::fromUtf8(xml));
        emit receivedMessage(id, message);

        nextBlockSize = 0;
    }
}

void Connection::onDisconnected() {
    emit disconnected(id);
}

void Connection::initializeSocket() {
    connect(socket, SIGNAL(connected()), this, SLOT(onConnected()));
    connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(onError(QAbstractSocket::SocketError)));
    connect(socket, SIGNAL(disconnected()), this, SLOT(onDisconnected()));
    connect(socket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
}

void Connection::open(const QHostAddress &hostAddress, quint16 port) {
    if(!socket->isOpen())
        socket->connectToHost(hostAddress, port);
}

void Connection::close() {
    if(socket->isOpen())
        socket->disconnectFromHost();
}

bool Connection::isOpen() const {
    return socket->isOpen();
}

void Connection::onConnected() {
    updateAddresses();
    emit connected();
}

void Connection::updateAddresses() {
    localAddress = socket->localAddress();
    peerAddress = socket->peerAddress();
}

void Connection::sendMessage(const Message &message) {
    QString xml = converter->toXmlString(message);

//    qDebug() << "Content: \n" << xml;

    if(sendData(xml.toUtf8()) == -1)
        qWarning() << "Connection: Failed to send message: " << message;
    qDebug() << "Connection: Message sent";
}
