#include "MessageConverter.h"
#include <QDomDocument>

#include <xmltags.h>

#include <iostream>

QString MessageConverter::toXmlString(const Message &message) const
{
    QDomDocument domDocument("MakaFo");

    int messageType = message.getType();

    QDomElement root = domDocument.createElement(XMLTag::mainTag);
    root.setAttribute(XMLTag::TypeAttrib::typeAttrib, messageType);
    domDocument.appendChild(root);

//    if (messageType < Message::Type::SERVER_PEERSYNC) {
        QDomElement dataElement = domDocument.createElement(XMLTag::textTag);
        dataElement.setAttribute(XMLTag::TextAttrib::textAttrib, message.getText());
        root.appendChild(dataElement);

        dataElement = domDocument.createElement(XMLTag::okTag);
        dataElement.setAttribute(XMLTag::OkAttrib::okAttrib, message.isOk());
        root.appendChild(dataElement);

        // id
        dataElement = domDocument.createElement(XMLTag::idTag);
        dataElement.setAttribute(XMLTag::IdAttrib::idAttrib, message.getID());
        root.appendChild(dataElement);

        // client
        dataElement = domDocument.createElement(XMLTag::clientTag);
        dataElement.setAttribute(XMLTag::ClientAttrib::ipAttrib, QString::fromStdString(message.getClientIP()));

        dataElement.setAttribute(XMLTag::ClientAttrib::portAttrib, message.getClientPort());
        root.appendChild(dataElement);

        //server
        dataElement = domDocument.createElement(XMLTag::serverTag);
        dataElement.setAttribute(XMLTag::ServerAttrib::ipAttrib, QString::fromStdString(message.getServerIP()));

        dataElement.setAttribute(XMLTag::ServerAttrib::portAttrib, message.getServerPort());
        root.appendChild(dataElement);

        //node
        dataElement = domDocument.createElement(XMLTag::nodeTag);
        dataElement.setAttribute(XMLTag::NodeAttrib::ipAttrib, QString::fromStdString(message.getNodeIP()));

        dataElement.setAttribute(XMLTag::NodeAttrib::portAttrib, message.getNodePort());
        root.appendChild(dataElement);

        // state
        dataElement = domDocument.createElement(XMLTag::stateTag);
        dataElement.setAttribute(XMLTag::StateAttrib::stateAttrib, message.getState());
        root.appendChild(dataElement);

        // query
        dataElement = domDocument.createElement(XMLTag::queryTag);
        dataElement.setAttribute(XMLTag::QueryAttrib::queryAttrib, QString::fromStdString(message.getQuery()));
        root.appendChild(dataElement);

        //response
        dataElement = domDocument.createElement(XMLTag::responseTag);
        dataElement.setAttribute(XMLTag::ResponseAttrib::responseAttrib, QString::fromStdString(message.getResponse()));
        root.appendChild(dataElement);

        //info
        dataElement = domDocument.createElement(XMLTag::infoTag);
        dataElement.setAttribute(XMLTag::InfoAttrib::infoAttrib, QString::fromStdString(message.getInfo()));
        root.appendChild(dataElement);

        //credentials
        dataElement = domDocument.createElement(XMLTag::credentialsTag);
        dataElement.setAttribute(XMLTag::CredentialsAttrib::loginAttrib, QString::fromStdString(message.getLogin()));
        dataElement.setAttribute(XMLTag::CredentialsAttrib::passwordAttrib, QString::fromStdString(message.getPassword()));
        root.appendChild(dataElement);

        // servers
        dataElement = domDocument.createElement(XMLTag::serversTag);
        std::list<Host> servers = message.getServers();
        if (!servers.empty()) {
            for (std::list<Host>::iterator it=servers.begin(); it != servers.end(); ++it) {
                QDomElement serverElement = domDocument.createElement(XMLTag::serverTag);
                serverElement.setAttribute(XMLTag::ServerAttrib::ipAttrib, QString::fromStdString(it->getAddress()));

                serverElement.setAttribute(XMLTag::ServerAttrib::portAttrib, it->getPort());
                dataElement.appendChild(serverElement);
            }
            root.appendChild(dataElement);
        }

//    } else if (messageType >= Message::Type::SERVER_PEERSYNC && messageType != Message::Type::SERVER_REQHSYNC) {
        // server
//        QDomElement dataElement = domDocument.createElement(XMLTag::serverTag);

//        dataElement.setAttribute(XMLTag::ServerAttrib::ipAttrib, QString::fromStdString(message.getServerIP()));
//        dataElement.setAttribute(XMLTag::ServerAttrib::portAttrib, message.getServerPort());
//        root.appendChild(dataElement);

//        dataElement = domDocument.createElement(XMLTag::credentialsTag);
//        dataElement.setAttribute(XMLTag::CredentialsAttrib::loginAttrib, QString::fromStdString(message.getLogin()));
//        dataElement.setAttribute(XMLTag::CredentialsAttrib::passwordAttrib, QString::fromStdString(message.getPassword()));
//        root.appendChild(dataElement);

        // sender
        dataElement = domDocument.createElement(XMLTag::senderTag);
        dataElement.setAttribute(XMLTag::SenderAttrib::ipAttrib, QString::fromStdString(message.getSenderIP()));

        dataElement.setAttribute(XMLTag::SenderAttrib::portAttrib, message.getSenderPort());
        root.appendChild(dataElement);
//    } else
//    if (messageType == Message::Type::SERVER_REQHSYNC) {
        // server
//        QDomElement dataElement = domDocument.createElement(XMLTag::serverTag);
//        dataElement.setAttribute(XMLTag::ServerAttrib::ipAttrib, QString::fromStdString(message.getServerIP()));
//
//        dataElement.setAttribute(XMLTag::ServerAttrib::portAttrib, message.getServerPort());
//        root.appendChild(dataElement);

        // sender
//        dataElement = domDocument.createElement(XMLTag::senderTag);
//        dataElement.setAttribute(XMLTag::SenderAttrib::ipAttrib, QString::fromStdString(message.getSenderIP()));
//
//        dataElement.setAttribute(XMLTag::SenderAttrib::portAttrib, message.getSenderPort());
//        root.appendChild(dataElement);

        // servers
//        dataElement = domDocument.createElement(XMLTag::serversTag);
//        std::list<Host> servers = message.getServers();
//        if (!servers.empty()) {
//            for (std::list<Host>::iterator it=servers.begin(); it != servers.end(); ++it) {
//                QDomElement serverElement = domDocument.createElement(XMLTag::serverTag);
//                serverElement.setAttribute(XMLTag::ServerAttrib::ipAttrib, QString::fromStdString(it->getAddress()));
//
//                serverElement.setAttribute(XMLTag::ServerAttrib::portAttrib, it->getPort());
//                dataElement.appendChild(serverElement);
//            }
//            root.appendChild(dataElement);
//        }
//        qDebug() << "sending -===================";
//        qDebug() << domDocument.toString() ; // TODO delete it
//        qDebug() << "sending -===================";
//    }

    return domDocument.toString();
}

Message MessageConverter::fromXmlString(const QString &xml) const
{
//    qDebug() << "Get XML" << xml;
    Message m;

    QString errorMsg;
    int errorLine, errorColumn;

    QDomDocument tree;

    bool result = tree.setContent(xml, &errorMsg, &errorLine, &errorColumn);

    if (!result) {
        // TODO: handling defective xml
    }

    QDomElement root = tree.documentElement();

    if (root.tagName() != XMLTag::mainTag) {
        // TODO: handling wrong xml
    }

    if (!root.hasAttribute(XMLTag::TypeAttrib::typeAttrib)) {
        // TODO: handling lack type attribute
    }

    int messageType = root.attribute(XMLTag::TypeAttrib::typeAttrib).toInt();

    m.setType(messageType);

    // if (messageType < Message::Type::SERVER_PEERSYNC) {
        QDomNodeList contentContainer;

        // text
        contentContainer = root.elementsByTagName(XMLTag::textTag);
        if (contentContainer.isEmpty()) {
            // TODO: handling text tag lack
        }

        QDomElement domElement = contentContainer.at(0).toElement();

        if (!domElement.hasAttribute(XMLTag::TextAttrib::textAttrib)) {
            // TODO:
        }
        m.setText(domElement.attribute(XMLTag::textTag));

        // ok
        contentContainer = root.elementsByTagName(XMLTag::okTag);
        if (contentContainer.isEmpty()) {
            // TODO: handling ok tag lack
        }

        domElement = contentContainer.at(0).toElement();

        if (!domElement.hasAttribute(XMLTag::OkAttrib::okAttrib)) {
            // TODO:
        }
        m.setOk(domElement.attribute(XMLTag::OkAttrib::okAttrib).toInt() != 0);

        // id
        contentContainer = root.elementsByTagName(XMLTag::idTag);
        if (contentContainer.isEmpty()) {
            // TODO: handling id tag lack
        }

        domElement = contentContainer.at(0).toElement();

        if (!domElement.hasAttribute(XMLTag::IdAttrib::idAttrib)) {
            // TODO:
        }
        m.setID(domElement.attribute(XMLTag::IdAttrib::idAttrib).toUInt());

        // client
        contentContainer = root.elementsByTagName(XMLTag::clientTag);
        if (contentContainer.isEmpty()) {
            // TODO: handling client tag lack
        }

        domElement = contentContainer.at(0).toElement();

        if (!domElement.hasAttribute(XMLTag::ClientAttrib::ipAttrib)) {
            // TODO:
        }
        m.setClientIP(domElement.attribute(XMLTag::ClientAttrib::ipAttrib).toStdString());

        if (!domElement.hasAttribute(XMLTag::ClientAttrib::portAttrib)) {
            // TODO:
        }
        m.setClientPort(domElement.attribute(XMLTag::ClientAttrib::portAttrib).toInt());

        // server
        contentContainer = root.elementsByTagName(XMLTag::serverTag);
        if (contentContainer.isEmpty()) {
            // TODO: handling server tag lack
        }

        domElement = contentContainer.at(0).toElement();

        if (!domElement.hasAttribute(XMLTag::ServerAttrib::ipAttrib)) {
            // TODO:
        }
        m.setServerIP(domElement.attribute(XMLTag::ServerAttrib::ipAttrib).toStdString());

        if (!domElement.hasAttribute(XMLTag::ServerAttrib::portAttrib)) {
            // TODO:
        }
        m.setServerPort(domElement.attribute(XMLTag::ServerAttrib::portAttrib).toInt());

        // node
        contentContainer = root.elementsByTagName(XMLTag::nodeTag);
        if (contentContainer.isEmpty()) {
            // TODO: handling node tag lack
        }

        domElement = contentContainer.at(0).toElement();

        if (!domElement.hasAttribute(XMLTag::NodeAttrib::ipAttrib)) {
            // TODO:
        }
        m.setNodeIP(domElement.attribute(XMLTag::NodeAttrib::ipAttrib).toStdString());

        if (!domElement.hasAttribute(XMLTag::NodeAttrib::portAttrib)) {
            // TODO:
        }
        m.setNodePort(domElement.attribute(XMLTag::NodeAttrib::portAttrib).toInt());

        // state
        contentContainer = root.elementsByTagName(XMLTag::stateTag);
        if (contentContainer.isEmpty()) {
            // TODO: handling state tag lack
        }

        domElement = contentContainer.at(0).toElement();

        if (!domElement.hasAttribute(XMLTag::StateAttrib::stateAttrib)) {
            // TODO:
        }
        m.setState(domElement.attribute(XMLTag::StateAttrib::stateAttrib).toInt());

        // query
        contentContainer = root.elementsByTagName(XMLTag::queryTag);
        if (contentContainer.isEmpty()) {
            // TODO: handling state tag lack
        }

        domElement = contentContainer.at(0).toElement();

        if (!domElement.hasAttribute(XMLTag::QueryAttrib::queryAttrib)) {
            // TODO:
        }
        m.setQuery(domElement.attribute(XMLTag::QueryAttrib::queryAttrib).toStdString());

        // response
        contentContainer = root.elementsByTagName(XMLTag::responseTag);
        if (contentContainer.isEmpty()) {
            // TODO: handling response tag lack
        }

        domElement = contentContainer.at(0).toElement();

        if (!domElement.hasAttribute(XMLTag::ResponseAttrib::responseAttrib)) {
            // TODO:
        }
        m.setResponse(domElement.attribute(XMLTag::ResponseAttrib::responseAttrib).toStdString());

        // info
        contentContainer = root.elementsByTagName(XMLTag::infoTag);
        if (contentContainer.isEmpty()) {
            // TODO: handling info tag lack
        }

        domElement = contentContainer.at(0).toElement();

        if (!domElement.hasAttribute(XMLTag::InfoAttrib::infoAttrib)) {
            // TODO:
        }
        m.setInfo(domElement.attribute(XMLTag::InfoAttrib::infoAttrib).toStdString());

        // credentials
        contentContainer = root.elementsByTagName(XMLTag::credentialsTag);
        if (contentContainer.isEmpty()) {
            // TODO: handling credentials tag lack
        }

        domElement = contentContainer.at(0).toElement();

        if (!domElement.hasAttribute(XMLTag::CredentialsAttrib::loginAttrib)) {
            // TODO:
        }
        m.setLogin(domElement.attribute(XMLTag::CredentialsAttrib::loginAttrib).toStdString());

        if (!domElement.hasAttribute(XMLTag::CredentialsAttrib::passwordAttrib)) {
            // TODO:
        }
        m.setPassword(domElement.attribute(XMLTag::CredentialsAttrib::passwordAttrib).toStdString());

        // servers
        contentContainer = root.elementsByTagName(XMLTag::serversTag);
        if (contentContainer.isEmpty()) {
            // TODO: handling servers tag lack
        }

        domElement = contentContainer.at(0).toElement();

        contentContainer = domElement.elementsByTagName(XMLTag::serverTag);

        for (int i=0; i<contentContainer.size(); ++i) {
            domElement = contentContainer.at(i).toElement();
            Host tempHost;

            if (!domElement.hasAttribute(XMLTag::ServerAttrib::ipAttrib)) {
                // TODO:
            }

            tempHost.setAddress(domElement.attribute(XMLTag::ServerAttrib::ipAttrib).toStdString());

            if (!domElement.hasAttribute(XMLTag::ServerAttrib::portAttrib)) {
                // TODO:
            }

            tempHost.setPort(domElement.attribute(XMLTag::ServerAttrib::portAttrib).toInt());

            m.addServer(tempHost);
        }

    // } else if (messageType >= Message::Type::SERVER_PEERSYNC && messageType != Message::Type::SERVER_REQHSYNC) {
        // QDomNodeList contentContainer;

        // server
        // contentContainer = root.elementsByTagName(XMLTag::serverTag);
        // if (contentContainer.isEmpty()) {
            // TODO: handling server tag lack
        // }

        // QDomElement domElement = contentContainer.at(0).toElement();

        // if (!domElement.hasAttribute(XMLTag::ServerAttrib::ipAttrib)) {
            // TODO:
        // }
        // m.setServerIP(domElement.attribute(XMLTag::ServerAttrib::ipAttrib).toStdString());

        // if (!domElement.hasAttribute(XMLTag::ServerAttrib::portAttrib)) {
            // TODO:
        // }
        // m.setServerPort(domElement.attribute(XMLTag::ServerAttrib::portAttrib).toInt());

        // credentials
        // contentContainer = root.elementsByTagName(XMLTag::credentialsTag);
        // if (contentContainer.isEmpty()) {
            // TODO: handling credentials tag lack
        // }

        // domElement = contentContainer.at(0).toElement();

        // if (!domElement.hasAttribute(XMLTag::CredentialsAttrib::loginAttrib)) {
            // TODO:
        // }
        // m.setLogin(domElement.attribute(XMLTag::CredentialsAttrib::loginAttrib).toStdString());

        // if (!domElement.hasAttribute(XMLTag::CredentialsAttrib::passwordAttrib)) {
            // TODO:
        // }
        // m.setPassword(domElement.attribute(XMLTag::CredentialsAttrib::passwordAttrib).toStdString());

        // sender
        contentContainer = root.elementsByTagName(XMLTag::senderTag);
        if (contentContainer.isEmpty()) {
            // TODO: handling sender tag lack
        }

        domElement = contentContainer.at(0).toElement();

        if (!domElement.hasAttribute(XMLTag::SenderAttrib::ipAttrib)) {
            // TODO:
        }
        m.setSenderIP(domElement.attribute(XMLTag::SenderAttrib::ipAttrib).toStdString());

        if (!domElement.hasAttribute(XMLTag::SenderAttrib::portAttrib)) {
            // TODO:
        }
        m.setSenderPort(domElement.attribute(XMLTag::SenderAttrib::portAttrib).toInt());

        // servers
        // contentContainer = root.elementsByTagName(XMLTag::serversTag);
        // if (contentContainer.isEmpty()) {
        //     // TODO: handling servers tag lack
        // }

        // domElement = contentContainer.at(0).toElement();

        // contentContainer = domElement.elementsByTagName(XMLTag::serverTag);

        // for (int i=0; i<contentContainer.size(); ++i) {
        //     domElement = contentContainer.at(i).toElement();
        //     Host tempHost;

        //     if (!domElement.hasAttribute(XMLTag::ServerAttrib::ipAttrib)) {
        //         // TODO:
        //     }

        //     tempHost.setAddress(domElement.attribute(XMLTag::ServerAttrib::ipAttrib).toStdString());

        //     if (!domElement.hasAttribute(XMLTag::ServerAttrib::portAttrib)) {
        //         // TODO:
        //     }

        //     tempHost.setPort(domElement.attribute(XMLTag::ServerAttrib::portAttrib).toInt());

        //     m.addServer(tempHost);
        // }

    // } else if (messageType == Message::Type::SERVER_REQHSYNC) {
    //     QDomNodeList contentContainer;

    //     // server
    //     contentContainer = root.elementsByTagName(XMLTag::serverTag);
    //     if (contentContainer.isEmpty()) {
    //         // TODO: handling server tag lack
    //     }

    //     QDomElement domElement = contentContainer.at(0).toElement();

    //     if (!domElement.hasAttribute(XMLTag::ServerAttrib::ipAttrib)) {
    //         // TODO:
    //     }
    //     m.setServerIP(domElement.attribute(XMLTag::ServerAttrib::ipAttrib).toStdString());

    //     if (!domElement.hasAttribute(XMLTag::ServerAttrib::portAttrib)) {
    //         // TODO:
    //     }
    //     m.setServerPort(domElement.attribute(XMLTag::ServerAttrib::portAttrib).toInt());

        // sender
        // contentContainer = root.elementsByTagName(XMLTag::senderTag);
        // if (contentContainer.isEmpty()) {
        //     // TODO: handling sender tag lack
        // }

        // domElement = contentContainer.at(0).toElement();

        // if (!domElement.hasAttribute(XMLTag::SenderAttrib::ipAttrib)) {
        //     // TODO:
        // }
        // m.setSenderIP(domElement.attribute(XMLTag::SenderAttrib::ipAttrib).toStdString());

        // if (!domElement.hasAttribute(XMLTag::SenderAttrib::portAttrib)) {
        //     // TODO:
        // }
        // m.setSenderPort(domElement.attribute(XMLTag::SenderAttrib::portAttrib).toInt());

        // servers
        // contentContainer = root.elementsByTagName(XMLTag::serversTag);
        // if (contentContainer.isEmpty()) {
        //     // TODO: handling servers tag lack
        // }

        // domElement = contentContainer.at(0).toElement();

        // contentContainer = domElement.elementsByTagName(XMLTag::serverTag);

        // for (int i=0; i<contentContainer.size(); ++i) {
        //     domElement = contentContainer.at(i).toElement();
        //     Host tempHost;

        //     if (!domElement.hasAttribute(XMLTag::ServerAttrib::ipAttrib)) {
        //         // TODO:
        //     }

        //     tempHost.setAddress(domElement.attribute(XMLTag::ServerAttrib::ipAttrib).toStdString());

        //     if (!domElement.hasAttribute(XMLTag::ServerAttrib::portAttrib)) {
        //         // TODO:
        //     }

        //     tempHost.setPort(domElement.attribute(XMLTag::ServerAttrib::portAttrib).toInt());

        //     m.addServer(tempHost);
        // }

    // }

    return m;
}
