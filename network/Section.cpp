#include "Section.h"

QDebug operator<<(QDebug qdb, const Section &s) {
    qdb << "\nSection ("
    << "name:" << QString::fromStdString(s.name)
    << "listen address:" << QString::fromStdString(s.listen.getAddress())
    << "listen port:" << s.listen.getPort();
    for (auto &host : s.send.hosts) {
        qdb << "\n" << host;
    }
    qdb << ")";
    return qdb;
}