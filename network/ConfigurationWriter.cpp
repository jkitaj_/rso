#include <QtCore/qfile.h>
#include "ConfigurationWriter.h"
#include "Configuration.h"

ConfigurationWriter::ConfigurationWriter() {
    xml.setAutoFormatting(true);
}

void ConfigurationWriter::saveConfigurationToFile(const QString &fileName, const Configuration &config) {
    QFile file(fileName);
    if(!file.open(QFile::WriteOnly | QFile::Text)) {
        qWarning() << "ConfigurationWriter: Failed to open file: " << fileName;
        return;
    }
    writeFile(&file, config);

    if(file.isOpen())
        file.close();
}

bool ConfigurationWriter::writeFile(QIODevice *device, const Configuration &config) {
    xml.setDevice(device);

    xml.writeStartDocument();
    xml.writeStartElement("configuration");
    for (auto &section : config.getSections()) {
        writeSection(section);
    }
    xml.writeEndElement();
    xml.writeEndDocument();

    return true;
}

void ConfigurationWriter::writeSection(Section &section) {
    xml.writeStartElement("section");
    xml.writeAttribute("name", QString::fromStdString(section.name));

    xml.writeEmptyElement("listen");
    writeHost(section.listen);

    xml.writeStartElement("send");
    for (auto host : section.send.hosts) {
        xml.writeEmptyElement("host");
        writeHost(host);
    }
    xml.writeEndElement();

    xml.writeEndElement();
}

void ConfigurationWriter::writeHost(const Host& host) {
    xml.writeAttribute("address", QString::fromStdString(host.getAddress()));
    xml.writeAttribute("port", QString::number(host.getPort()));
}
