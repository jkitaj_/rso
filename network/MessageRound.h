#ifndef RSO_MESSAGEROUND_H
#define RSO_MESSAGEROUND_H

#include "Message.h"
#include <thread>     
#include <mutex>         
#include <condition_variable>
#include <iostream>



class MessageRound{

	private:
		static const int MAXIMUM=10;
		Message buffer[MAXIMUM];
		int in_buffer;
		std::mutex mtx;             // mutex for critical section
		std::condition_variable cv; 
		
	public:
		MessageRound(): in_buffer(0){ 
			for(int i=0; i <MAXIMUM; i++){
				buffer[i]=Message(-1);
			}
		}
		int countBuffer(){
			int count=0;
			int i=0;
			std::unique_lock<std::mutex> lck(mtx);
			while(i<MAXIMUM){ 
				if(buffer[i].getType()!=-1){
					count++;
				} else { break;} 
				i++;
			}
			return count;
		}
		void setMessage(Message message){
			std::unique_lock<std::mutex> lck(mtx);
 			while(in_buffer>=this->MAXIMUM){ 
 				std::cout<<"Message Buffor full waiting..."<<std::endl;
 				cv.wait(lck); 
 			}
					
			buffer[in_buffer]=message;
			in_buffer++;
			std::cout<<"Adding message to buffer on position "<<in_buffer<<std::endl;
			cv.notify_all(); 
		}
		void getMessage(Message& message){
			std::unique_lock<std::mutex> lck(mtx);
			while(in_buffer<=0){ 
 				std::cout<<"Message Buffor empty waiting..."<<std::endl;
 				cv.wait(lck); 
 			}
 			message=buffer[in_buffer-1];
 			buffer[in_buffer-1]=Message(-1);
 			--in_buffer;
 			std::cout<<"Message removed from buffer, "<<in_buffer<<" left to take."<<std::endl;
 			cv.notify_all(); 
		}

};

#endif // RSO_MESSAGEROUND_H
