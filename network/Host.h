#ifndef RSO_HOST_H
#define RSO_HOST_H

#include <stdint.h>
#include <string>
#include <QDebug>
#include <ostream>

class Host {
public:
    Host() : address(""), port(0) {}
    Host(const std::string& address_, uint16_t port_) : address(address_), port(port_) {}

    std::string getAddress() const {
        return address;
    }

    void setAddress(const std::string& address) {
        this->address = address;
    }

    uint16_t getPort() const {
        return port;
    }

    void setPort(uint16_t port) {
        this->port = port;
    }

    bool operator==(const Host& other) {
        return (address == other.address) && (port == other.port);
    }

    unsigned getLastPartAddress() const {
        if (address != "") {
            return std::stoi(address.substr(address.find_last_of('.') + 1));
        }
    }

private:
    std::string address;
    uint16_t port;
};

QDebug operator<<(QDebug qdb, const Host &h);
std::ostream & operator<<(std::ostream &out, const Host &h);

#endif //RSO_HOST_H
