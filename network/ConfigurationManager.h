#ifndef RSO_CONFIGURATIONMANAGER_H
#define RSO_CONFIGURATIONMANAGER_H

#include <QString>
#include <string>
#include <list>
#include "Configuration.h"
#include "Host.h"

class ConfigurationLoader;
class ConfigurationWriter;

class ConfigurationManager {
public:
    ConfigurationManager(const std::string& configFileName);
    virtual ~ConfigurationManager();

    Configuration getCurrentConfiguration();
    std::list<Host> getHostsFromSection(const std::string& sectionName);
    void addHostToSection(const Host& host, const std::string& sectionName);
    void addHostsToSection(const std::list<Host>& hosts, const std::string& sectionName);
    void setListenHost(const Host& host, const std::string& sectionName);
    void removeHostFromSection(const Host& host, const std::string &sectionName);
    void removeAllHostsFromSection(const std::string &sectionName);

private:
    QString fileName;
    Configuration currentConfig;
    ConfigurationLoader *configLoader;
    ConfigurationWriter *configWriter;

    bool reloadConfiguration();
    void saveConfigurationToFile();
};

#endif //RSO_CONFIGURATIONMANAGER_H
