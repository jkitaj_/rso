#ifndef RSO_CONFIGURATIONWRITER_H
#define RSO_CONFIGURATIONWRITER_H

#include <QtCore/qxmlstream.h>
#include "Section.h"

class Configuration;
class QString;
class QIODevice;

class ConfigurationWriter {
public:
    ConfigurationWriter();
    void saveConfigurationToFile(const QString& fileName, const Configuration& config);

private:
    QXmlStreamWriter xml;

    bool writeFile(QIODevice *device, const Configuration &config);
    void writeSection(Section & section);
    void writeHost(const Host& host);
};

#endif //RSO_CONFIGURATIONWRITER_H
