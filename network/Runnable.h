#ifndef RSO_RUNNABLE_H
#define RSO_RUNNABLE_H


#include <thread>
#include <iostream>



class Runnable{
public:
    void start(){
        if(run_==false){
            run_=true;
            t = std::thread(&Runnable::run, this);
        }else{
            std::cout<<"Object already is runing"<<std::endl;
        }
    }
    void stop(){run_=false; };
    explicit Runnable(){ run_=false;	}
    ~Runnable() {};
protected:
    virtual void run()=0;
    volatile bool run_;
private:
    std::thread t;
};

#endif
