#include "ConfigurationManager.h"
#include "ConfigurationLoader.h"
#include "ConfigurationWriter.h"

ConfigurationManager::ConfigurationManager(std::string const &configFileName)
    : fileName(QString::fromStdString(configFileName))
{
    configLoader = new ConfigurationLoader();
    configWriter = new ConfigurationWriter();

    if(reloadConfiguration())
        qDebug() << "\n" << currentConfig << "\n";
}

ConfigurationManager::~ConfigurationManager() {
    delete configLoader;
    delete configWriter;
}

Configuration ConfigurationManager::getCurrentConfiguration() {
    return currentConfig;
}

std::list<Host> ConfigurationManager::getHostsFromSection(const std::string &name) {
    try {
        Section section = currentConfig.getSectionByName(name);
        return section.send.hosts;
    } catch(const std::invalid_argument& e) {
        qWarning() << "ConfigurationManager: " << e.what();
    }
    return std::list<Host>();
}

void ConfigurationManager::addHostToSection(const Host &host, const std::string &section) {
    try {
        if(currentConfig.addHostToSection(host, section)) {
            saveConfigurationToFile();
        }
        else qDebug() << "ConfigurationManager:" << host << "not added - already existed";
    } catch(const std::invalid_argument& e) {
        qWarning() << "ConfigurationManager: " << e.what();
    }
}

void ConfigurationManager::addHostsToSection(const std::list<Host> &hosts, const std::string &sectionName) {
    try {
        if(currentConfig.addHostsToSection(hosts, sectionName)) {
            saveConfigurationToFile();
        }
        else qDebug() << "ConfigurationManager: hosts not added - already existed";
    } catch(const std::invalid_argument& e) {
        qWarning() << "ConfigurationManager: " << e.what();
    }
}

void ConfigurationManager::setListenHost(const Host &host, const std::string &sectionName) {
    try {
        if(currentConfig.setListenHost(host, sectionName)) {
            saveConfigurationToFile();
        }
    } catch(const std::invalid_argument& e) {
        qWarning() << "ConfigurationManager: " << e.what();
    }
}

void ConfigurationManager::removeHostFromSection(const Host &host, const std::string &section) {
    try {
        if(currentConfig.removeHostFromSection(host, section)) {
            saveConfigurationToFile();
        }
        else qDebug() << "ConfigurationManager:" << host << "not removed - not found on list";
    } catch(const std::invalid_argument& e) {
        qWarning() << "ConfigurationManager: " << e.what();
    }
}

void ConfigurationManager::removeAllHostsFromSection(const std::string &section) {
    try {
        if(currentConfig.removeAllHostsFromSection(section)) {
            saveConfigurationToFile();
        }
        else qDebug() << "ConfigurationManager: no host removed - list was already empty";
    } catch(const std::invalid_argument& e) {
        qWarning() << "ConfigurationManager: " << e.what();
    }
}

bool ConfigurationManager::reloadConfiguration() {
    return configLoader->loadConfiguration(fileName, currentConfig);
}

void ConfigurationManager::saveConfigurationToFile() {
    configWriter->saveConfigurationToFile(fileName, currentConfig);
}

