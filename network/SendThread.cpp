#include <QtNetwork/qtcpsocket.h>
#include <QtNetwork/qhostaddress.h>
#include "SendThread.h"
#include "MessageConverter.h"
#include "constants.h"

SendThread::SendThread(const Host receiver, const Message message, QObject *parent) : QThread(parent) {
    this->receiver = receiver;
    this->message = message;
    converter = new MessageConverter();
}

void SendThread::run() {
    QString qtAddress = QString::fromStdString(receiver.getAddress());
    QHostAddress addr(qtAddress);
    if(addr == QHostAddress::Null) {
        qWarning() << "SendThread: Wrong address format: " << qtAddress;
        emit error(receiver, message);
        return;
    }

    QTcpSocket socket;
    socket.connectToHost(addr, receiver.getPort());
    if (!socket.waitForConnected(TIMEOUT)) {
        qWarning() << "SendThread:" << socket.errorString();
        emit error(receiver, message);
        return;
    }

    QString xml = converter->toXmlString(message);
    if(sendData(&socket, xml.toUtf8()) == -1) {
        qWarning() << "SendThread: Failed to send message: " << message << "to" << receiver;
        emit error(receiver, message);
        return;
    }

    socket.disconnectFromHost();
    if(!socket.waitForDisconnected(TIMEOUT)) {
        qWarning() << "SendThread: Error on disconnecting: " << socket.errorString();
        emit error(receiver, message);
        return;
    }
    emit success(receiver, message);
}

qint64 SendThread::sendData(QTcpSocket *socket, const QByteArray &message) {
    QByteArray data;
    QDataStream out(&data, QIODevice::WriteOnly);
    out.setVersion(dataStreamVersion);
    out << MessageHeader(0);

    out << message;
    out.device()->seek(0);
    out << MessageHeader(data.size() - sizeof(MessageHeader));

    return socket->write(data); // Returns the number of bytes that were written, or -1 if an error occurred
}
