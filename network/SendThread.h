#ifndef RSO_SENDTHREAD_H
#define RSO_SENDTHREAD_H

#include <QThread>
#include "Host.h"
#include "Message.h"

class MessageConverter;
class QTcpSocket;

class SendThread : public QThread {
    Q_OBJECT

public:
    SendThread(const Host receiver, const Message message, QObject *parent = 0);

protected:
    virtual void run();

private:
    Host receiver;
    Message message;
    MessageConverter *converter;

    qint64 sendData(QTcpSocket *socket, const QByteArray& message);

signals:
    void error(Host receiver, Message message);
    void success(Host receiver, Message message);
};

#endif //RSO_SENDTHREAD_H
