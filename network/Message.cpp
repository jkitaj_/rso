#include "Message.h"
#include <iostream>

QDebug operator<<(QDebug qdb, const Message& m) {
    qdb
    << "Message ("
    << "sender:" << QString::fromStdString(m.getSenderIP()) << ":" << m.getSenderPort()
    << "server:" << QString::fromStdString(m.getServerIP()) << ":" << m.getServerPort()
    << "type:" << m.getType()
    << "text:" << m.getText()
    << "ok:" << m.isOk()
    << "id:" << m.getID()
    << "query:" << QString::fromStdString(m.getQuery())
    << "response:" << QString::fromStdString(m.getResponse())
    << ")";
    return qdb;
}

Message::Message(Message::Type type_, const QString& text_, bool ok_, uint id_, Message::State state_,
                 std::string clientIP_, std::string serverIP_, std::string nodeIP_, std::string senderIP_, uint16_t senderPort_)
        : type(type_),
          text(text_),
          ok(ok_),
          id(id_),
          state(state_),
          clientIP(clientIP_),
          serverIP(serverIP_),
          nodeIP(nodeIP_),
          senderIP(senderIP_),
          senderPort(senderPort_)
{
}

Message::Message(int type_, const QString& text_, bool ok_, uint id_, Message::State state_,
                 std::string clientIP_, std::string serverIP_, std::string nodeIP_, std::string senderIP_, uint16_t senderPort_)
        : Message(static_cast<Message::Type>(type), text_, ok_, id_, state_, clientIP_,
                  serverIP_, nodeIP_, senderIP_, senderPort_)
{
}

int Message::getType() const {
    return type;
}

void Message::setType(Type type)
{
    this->type = type;
}

void Message::setType(int type) {
    if (type >= Message::Type::UNDEFINED)
        this->type = static_cast<Message::Type >(type);
}

QString Message::getText() const {
    return text;
}

void Message::setText(const QString& text) {
    this->text = text;
}

bool Message::isOk() const {
    return ok;
}

void Message::setOk(bool ok) {
    this->ok = ok;
}

bool Message::operator ==(const Message &other) const {
    return this->type == other.type
           && this->text == other.text
           && this->ok == other.ok;
}

bool Message::operator !=(const Message &other) const {
    return !(*this == other);
}

void Message::setID(uint id) {
    this->id = id;
}

void Message::setClientIP(std::string clientIP) {
    this->clientIP = clientIP;

}

void Message::setServer(const Host host) {
    setServerIP(host.getAddress());
    setServerPort(host.getPort());
}

void Message::setSender(const Host host) {
    setSenderIP(host.getAddress());
    setSenderPort(host.getPort());
}

void Message::setServerIP(std::string serverIP) {
    this->serverIP = serverIP;
}

void Message::setSenderIP(std::string senderIP) {
    this->senderIP = senderIP;
}

void Message::setNodeIP(std::string nodeIP) {
    this->nodeIP = nodeIP;
}

void Message::setClientPort(uint16_t clientPort)
{
    this->clientPort = clientPort;
}

void Message::setServerPort(uint16_t serverPort)
{
    this->serverPort = serverPort;
}

void Message::setSenderPort(uint16_t senderPort)
{
    this->senderPort = senderPort;
}

void Message::setNodePort(uint16_t nodePort)
{
    this->nodePort = nodePort;
}

void Message::setState(Message::State state) {
    this->state = state;
}

void Message::setState(int state) {
    this->state = Message::State(state);
}

void Message::setQuery(std::string query) {
    this->query = query;
}

void Message::setResponse(std::string response) {
    this->response = response;
}

void Message::setInfo(std::string info) {
    this->info = info;
}

void Message::setLogin(std::string login) {
    this->login = login;
}

void Message::setPassword(std::string password) {
    this->password = password;
}

void Message::print() const {

std::cout<<"_________ Message ______________\n"
		 <<"Client IP ="<<clientIP<<"\t client port= "<<clientPort<<std::endl
		 <<"Server IP ="<<serverIP<<"\t server port= "<<clientPort<<std::endl
		 <<"Node IP ="<<nodeIP<<"\t node port ="<<nodePort<<std::endl
		 <<"Query ="<<query<<"\t state="<<state<<"\n response \t"
		 <<response<<"\n"<<"info:"<<info<<std::endl
		 <<"Login ="<<login<<"\t Password="<<password<<"\n";
}

QString Message::getMessageType() const
{
    QString type_;
    switch (this->type) {
        case Message::Type::UNDEFINED:
            type_ = "UNDEFINED";
            break;
        case Message::Type::CLIENT_REQUEST:
            type_ = "CLIENT_REQUEST";
            break;
        case Message::Type::CLIENT_REQHSYNC:
            type_ = "CLIENT_REQHSYNC";
            break;
        case Message::Type::SERVER_PEERSYNC:
            type_ = "SERVER_PEERSYNC";
            break;
        case Message::Type::SERVER_REQADD:
            type_ = "SERVER_REQADD";
            break;
        case Message::Type::SERVER_REQADDING:
            type_ = "SERVER_REQADDING";
            break;
        case Message::Type::SERVER_REQHSYNC:
            type_ = "SERVER_REQHSYNC";
            break;
        case Message::Type::SERVER_RESPADDED:
            type_ = "SERVER_RESPADDED";
            break;
        case Message::Type::SERVER_RESADDING:
            type_ = "SERVER_RESADDING";
            break;
        case Message::Type::SERVER_RESPAUTH:
            type_ = "SERVER_RESPAUTH";
            break;
        case Message::Type::SERVER_RESPNAUTH:
            type_ = "SERVER_RESPNAUTH";
            break;
        case Message::Type::SERVER_REQELEC:
            type_ = "SERVER_REQELEC";
            break;
        case Message::Type::SERVER_RESPELECSORRY:
            type_ = "SERVER_RESPELECSORRY";
            break;
        case Message::Type::SERVER_RESPNOTLEADER:
            type_ = "SERVER_RESPNOTLEADER";
            break;
        case Message::Type::SERVER_NODERESPDBADDRESS:
            type_ = "SERVER_NODERESPDBADDRESS";
            break;
        case Message::Type::NODE_GETDBADDRESS:
            type_ = "NODE_GETDBADDRESS";
            break;
        default:
            type_ = "?|?|?|?|?|?";
            break;
    }

    return type_;
}
