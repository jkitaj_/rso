#include "Configuration.h"
#include <stdexcept>

QDebug operator<<(QDebug qdb, const Configuration &c){
   qdb << "Configuration (";
   for (auto &s : c.sections) {
      qdb << s;
   }
   qdb << ")";
   return qdb;
}

void Configuration::addSection(Section section) {
   sections.push_back(section);
}

Section Configuration::getSectionByName(const std::string &name) const {
   for (auto &s : sections) {
      if(s.name == name) {
          return s;
      }
   }
   throw std::invalid_argument("Configuration: No such section: " + name);
}

bool Configuration::addHostToSection(const Host& host, const std::string& name) {
   for (auto &s : sections) {
      if(s.name == name) {
         return s.send.addHost(host);
      }
   }
   throw std::invalid_argument("Configuration: No such section: " + name);
}

bool Configuration::addHostsToSection(const std::list<Host> &hosts, const std::string &name) {
   for (auto &s : sections) {
      if(s.name == name) {
         for (auto host : hosts) {
            s.send.addHost(host);
         }
         return true;
      }
   }
   throw std::invalid_argument("Configuration: No such section: " + name);
}

bool Configuration::setListenHost(const Host& host, const std::string& name) {
   for (auto &s : sections) {
      if(s.name == name) {
         s.listen.setAddress(host.getAddress());
         s.listen.setPort(host.getPort());
         return true;
      }
   }
   throw std::invalid_argument("Configuration: No such section: " + name);
}

bool Configuration::removeHostFromSection(const Host &host, const std::string &name) {
   for (auto &s : sections) {
      if(s.name == name) {
         return s.send.removeHost(host);
      }
   }
   throw std::invalid_argument("Configuration: No such section: " + name);
}

bool Configuration::removeAllHostsFromSection(const std::string &name) {
   for (auto &s : sections) {
      if(s.name == name) {
         return s.send.removeAllHosts();
      }
   }
   throw std::invalid_argument("Configuration: No such section: " + name);
}

std::list<Section> Configuration::getSections() const {
   return sections;
}

