#ifndef RSO_COMMUNICATIONMODULE_H
#define RSO_COMMUNICATIONMODULE_H

#include <QObject>
#include <QtNetwork/qtcpserver.h>
#include "Message.h"
#include "Configuration.h"
#include "Section.h"
#include "ConfigurationManager.h"
#include <memory>
#include <QHash>
#include <atomic>

class Connection;
class ConfigurationLoader;

class CommunicationModule : public QObject {
    Q_OBJECT

public:
    typedef std::shared_ptr<ConfigurationManager> ConfigManagerPtr;

    explicit CommunicationModule(const std::string& configFileName, QObject *parent = 0);
    explicit CommunicationModule(ConfigManagerPtr manager, QObject *parent = 0);
    virtual ~CommunicationModule();

    void listenToConnectionsFrom(const std::string &section);
    void stopListening();

    // Send methods below should be thread-safe
    void send(const Host host, const Message& message);
    void send(const std::string& address, uint16_t port, const Message& message);

    uint16_t getListenPort() const;
    std::string getListenAddress() const;

protected:
    bool sendAndWait(const Host &receiver, const Message &message);
    virtual void onReceive(Message& message) { }
    virtual void onMessageSent(const Host& receiver, const Message& message) { }
    virtual void onFailedToSendMessage(const Host& receiver, const Message& message) { }
    ConfigManagerPtr getConfigurationManager();

private:
    QTcpServer *tcpServer;
    std::shared_ptr<ConfigurationManager> configManager;
    std::atomic<int> sendThreadCount;
    QHash<long, Connection*> connections;

    void listen(const std::string &address, uint16_t port);
    void listen(const QHostAddress& address, uint16_t port);
    void listen(const Host);

    qint64 sendData(QTcpSocket *socket, const QByteArray& message);

private slots:
    void onReceiveConnectionLost(const long);
    void onAcceptError(QAbstractSocket::SocketError);
    void onNewConnection();
    void onReceivedMessage(const long id, Message& message);
    void onSendSuccess(Host receiver, Message message);
    void onSendError(Host receiver, Message message);
};

#endif //RSO_COMMUNICATIONMODULE_H
