#ifndef RSO_CONSTANTS_H
#define RSO_CONSTANTS_H

const unsigned int TIMEOUT = 10000; // timeout for connection and disconnection
const QDataStream::Version dataStreamVersion = QDataStream::Qt_5_0;
typedef quint16 MessageHeader;

#endif //RSO_CONSTANTS_H
