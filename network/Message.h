#ifndef RSO_MESSAGE_H
#define RSO_MESSAGE_H

#include <QString>
#include <QtCore/qdebug.h>
#include "Host.h"

// TODO: add sender member

class Message {
public:
    enum Type {
        UNDEFINED=-1,
        CLIENT_REQUEST,
        CLIENT_REQHSYNC, // HOSTS SYNC

        // == SERVER
        // REQUEST
        SERVER_PEERSYNC=33, // i'm allowed because i'm doing this for science
        SERVER_REQADD,
        SERVER_REQADDING,
        SERVER_REQHSYNC, // HOSTS SYNC
        SERVER_REQNLEADER, // NEW LEADER
        SERVER_REQELEC, // ELECTION

        // RESPONSE to server
        SERVER_RESPADDED,
        SERVER_RESADDING,
        SERVER_RESPAUTH, // AUTHENTICATED
        SERVER_RESPNAUTH, // NO AUTHENTICATED
        SERVER_RESPELECSORRY, // ELECTION SORRY
        SERVER_RESPNOTLEADER, // I AM NOT A LEADER

        // RESPONSE to node
        SERVER_NODERESPDBADDRESS,

        SERVER_LIST_TO_CHECK,   // Slave sends this message to leader, message contains list of servers to check
        SERVER_CHECK_IF_ALIVE,  // Server sends this message to other server to check if it's alive

        // == NODE
        //REQUEST
        NODE_GETDBADDRESS = 66,

        // RESPONSE
    };

    enum State {
        UNINITIALIZED=0,
        SENT=10,
        INQUEUE=20,
        INPROGRES=30,
        READY=40,
        SENDBACK=50,
        ERROR=60
    };

    Message(Type type = Type::UNDEFINED, const QString& text = "", bool ok = true,
            uint id = 0, Message::State state = Message::State::UNINITIALIZED,
            std::string clientIP = "0",
            std::string serverIP = "0",
            std::string nodeIP = "0",
            std::string senderIP = "0",
            uint16_t senderPort = 0
    );

    Message(int type, const QString& text = "", bool ok = true,
            uint id = 0, Message::State state = Message::State::UNINITIALIZED,
            std::string clientIP = "0",
            std::string serverIP = "0",
            std::string nodeIP = "0",
            std::string senderIP = "0",
            uint16_t senderPort = 0
    );


    int getType() const;
    void setType(int type);
    void setType(Type type);

    QString getText() const;
    void setText(const QString& text);

    void setID(uint id);
    void setClientIP(std::string clientIP);
    void setServer(const Host host);
    void setServerIP(std::string serverIP);
    void setServerPort(uint16_t serverPort);
    void setSender(const Host host);
    void setSenderIP(std::string serverIP);
    void setSenderPort(uint16_t serverPort);
    void setNodeIP(std::string nodeIP);
    void setClientPort(uint16_t clientPort);
    void setNodePort(uint16_t nodePort);
    void setState(Message::State state);
    void setState(int state);
    void setQuery(std::string query);
    void setResponse(std::string response);
    void setInfo(std::string info);
    void setLogin(std::string login);
    void setPassword(std::string password);

    uint getID() const { return id; }
    std::string getClientIP() const { return clientIP; }
    std::string getServerIP() const { return serverIP; }
    std::string getNodeIP() const { return nodeIP; }
    uint16_t getClientPort() const { return clientPort; }
    uint16_t getServerPort() const { return serverPort; }
    uint16_t getNodePort() const { return nodePort; }
    Host getServer() const { return Host(getServerIP(), getServerPort()); }
    Host getSender() const { return Host(getSenderIP(), getSenderPort()); }
    std::string getSenderIP() const { return senderIP; }
    uint16_t getSenderPort() const { return senderPort; }
    Message::State getState() const { return state; }
    std::string getQuery() const { return query; }
    std::string getResponse() const { return response; }
    std::string getInfo() const { return info; }
    std::string getPassword() const { return password; }
    std::string getLogin() const { return login; }
    QString getMessageType() const;

    std::list<Host> const &getServers() const { return servers; }
    void addServer(const Host& server) { servers.push_back(server); }
    void appendServers(const std::list<Host>& servers_) { servers.insert(servers.end(), servers_.begin(), servers_.end()); }
    void addServers(const std::list<Host>& servers_) { clearServers(); appendServers(servers_); }
    void clearServers() { servers.clear(); }

    bool isOk() const;
    void setOk(bool ok);
    void print() const;

    bool operator ==(const Message &other) const;
    bool operator !=(const Message &other) const;

private:
    Type type;
    QString text;
    bool ok;

    uint id;
    std::string clientIP;
    std::string serverIP;
    std::string nodeIP;
    uint16_t clientPort;
    uint16_t serverPort;
    uint16_t nodePort;

    std::string senderIP;
    uint16_t senderPort;

    Message::State state;

    std::string query;
    std::string response;
    std::string info;
    std::string login;
    std::string password;

    std::list<Host> servers;
};

QDebug operator<<(QDebug qdb, const Message& m);

#endif //RSO_MESSAGE_H
