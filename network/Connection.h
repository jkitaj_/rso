#ifndef RSO_CONNECTION_H
#define RSO_CONNECTION_H

#include <QObject>
#include <QtNetwork/qhostaddress.h>

class Message;
class QTcpSocket;
class MessageConverter;

class Connection : public QObject {
    Q_OBJECT
    typedef quint16 MessageHeader;

public:
    explicit Connection(QObject *parent = 0);
    Connection(QTcpSocket *socket, QObject *parent = 0);
    virtual ~Connection();

    void open(const QHostAddress& hostAddress, quint16 port);
    void close();
    bool isOpen() const;

    void sendMessage(const Message& message);

    long getId() const { return id; }
    QHostAddress getLocalAddress() const { return localAddress; }
    QHostAddress getPeerAddress() const { return peerAddress; }

private:
    static long lastId;
    const long id;
    QTcpSocket *socket;
    MessageHeader nextBlockSize;
    QHostAddress localAddress;
    QHostAddress peerAddress;
    MessageConverter *converter;

    qint64 sendData(const QByteArray& message);
    void initializeSocket();
    void updateAddresses();

signals:
    void receivedMessage(const long connectionId, Message& message);
    void disconnected(const long connectionId);
    void connected();

private slots:
    void onError(QAbstractSocket::SocketError);
    void onReadyRead();
    void onConnected();
    void onDisconnected();
};

#endif //RSO_CONNECTION_H
