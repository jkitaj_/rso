#include <QtCore/qthread.h>
#include "CommunicationModule.h"
#include "Connection.h"
#include "ConfigurationLoader.h"
#include "SendThread.h"
#include "MessageConverter.h"
#include <QTcpSocket>
#include "constants.h"

CommunicationModule::CommunicationModule(const std::string &configFileName, QObject *parent)
        : CommunicationModule(ConfigManagerPtr(new ConfigurationManager(configFileName)), parent) {}

CommunicationModule::CommunicationModule(ConfigManagerPtr manager, QObject *parent)
        : QObject(parent), sendThreadCount(0)
{
    tcpServer = new QTcpServer(this);
    connect(tcpServer, SIGNAL(acceptError(QAbstractSocket::SocketError)), this, SLOT(onAcceptError(QAbstractSocket::SocketError)));
    // whenever a user connects, it will emit signal
    connect(tcpServer, SIGNAL(newConnection()), this, SLOT(onNewConnection()));

    configManager = manager;

    // Register class Host so its instances can be used as arguments in signals and slots
    qRegisterMetaType<Host>("Host");
    qRegisterMetaType<Message>("Message");
}

CommunicationModule::~CommunicationModule() {
    for(auto it = connections.begin(); it != connections.end(); ++it) {
        (*it)->close();
    }
    tcpServer->close();
}

void CommunicationModule::send(const Host host, const Message &message) {
    qDebug() << "\nCommunicationModule: I will send message " << message
        << "to" << QString::fromStdString(host.getAddress()) << "port" << host.getPort();

    SendThread *sendThread = new SendThread(host, message);
    connect(sendThread, SIGNAL(success(Host, Message)), this, SLOT(onSendSuccess(Host, Message)));
    connect(sendThread, SIGNAL(error(Host, Message)), this, SLOT(onSendError(Host, Message)));
    connect(sendThread, SIGNAL(finished()), sendThread, SLOT(deleteLater()));

    ++sendThreadCount;
    qDebug() << "CommunicationModule: Active send connections: " << sendThreadCount;

    sendThread->start();
}

void CommunicationModule::send(const std::string &address, uint16_t port, const Message &message) {
    send(Host(address, port), message);
}

void CommunicationModule::listenToConnectionsFrom(const std::string &section) {
    try {
        Q_ASSERT(configManager != nullptr);
        Section s = configManager->getCurrentConfiguration().getSectionByName(section);
        if(s.listen.getAddress() == "any") {
            listen(QHostAddress::Any, s.listen.getPort());
        } else {
            listen(s.listen.getAddress(), s.listen.getPort());
        }
    }
    catch(const std::invalid_argument& e) {
        qWarning() << "CommunicationModule: Failed to listen for section: " << QString::fromStdString(section) << e.what();
    }
}

void CommunicationModule::listen(const std::string &address, uint16_t port) {
    QString qtAddress = QString::fromStdString(address);
    QHostAddress addr(qtAddress);
    if(addr == QHostAddress::Null) {
        qWarning() << "CommunicationModule: Wrong address format:" << qtAddress;
        qWarning() << "CommunicationModule: Failed to listen on address " << qtAddress << " and port " << port;
        return;
    }
    listen(addr, port);
}

void CommunicationModule::listen(const QHostAddress &address, uint16_t port) {
    if(tcpServer->listen(address, port)) {
        qDebug() << "CommunicationModule: Listening on address " << address << " and port " << port;
    } else {
        qWarning() << "CommunicationModule: Failed to listen on address " << address << " and port " << port;
    }
}

void CommunicationModule::listen(const Host host) {
    listen(host.getAddress(), host.getPort());
}

void CommunicationModule::onAcceptError(QAbstractSocket::SocketError error) {
    qWarning() << "CommunicationModule: AcceptError: " << error;
}

void CommunicationModule::onNewConnection() {
    QTcpSocket *socket = tcpServer->nextPendingConnection();

    if(socket != nullptr) {

        Connection *connection = new Connection(socket, this);

        connect(connection, SIGNAL(disconnected(const long)), this, SLOT(onReceiveConnectionLost(const long)));
        connect(connection, SIGNAL(receivedMessage(const long, Message& )), this, SLOT(onReceivedMessage(const long, Message& )));

        connections[connection->getId()] = connection;

        qDebug() << "CommunicationModule: New peer connected: " << connection->getPeerAddress()
            << " Total receiver connections: " << connections.size();
    }
}

void CommunicationModule::onReceivedMessage(const long connectionId, Message &message) {
    qDebug() << "CommunicationModule: Received message:" << message;
    onReceive(message);
    if(connections.contains(connectionId)) {
        Connection *connection = connections.take(connectionId);
        connection->close();
        connection->deleteLater();
        qDebug() << "CommunicationModule: Active receiver connections: " << connections.size();
    }
}

void CommunicationModule::onReceiveConnectionLost(const long connectionId) {
    if(connections.contains(connectionId)) {
        Connection *connection = connections.take(connectionId);
        qDebug() << "CommunicationModule: Lost connection with" << connection->getPeerAddress();
        connection->deleteLater();
        qDebug() << "CommunicationModule: Active receiver connections: " << connections.size();
    }
}

uint16_t CommunicationModule::getListenPort() const {
    return tcpServer->serverPort(); // Returns the server's port if the server is listening for connections; otherwise returns 0.
}

std::string CommunicationModule::getListenAddress() const {
    return tcpServer->serverAddress().toString().toStdString();
}

CommunicationModule::ConfigManagerPtr CommunicationModule::getConfigurationManager() {
    return this->configManager;
}

void CommunicationModule::onSendSuccess(Host receiver, Message message) {
    --sendThreadCount;
    qDebug() << "CommunicationModule: Message sent. Active send connections: " << sendThreadCount;
    onMessageSent(receiver, message);
}

void CommunicationModule::onSendError(Host receiver, Message message) {
    --sendThreadCount;
    qDebug() << "CommunicationModule: Failed to send message " << message << "Active send connections: " << sendThreadCount;
    onFailedToSendMessage(receiver, message);
}

bool CommunicationModule::sendAndWait(const Host &receiver, const Message &message) {

    QString qtAddress = QString::fromStdString(receiver.getAddress());
    QHostAddress addr(qtAddress);
    if(addr == QHostAddress::Null) {
        qWarning() << "CommunicationModule: Wrong address format: " << qtAddress;
        return false;
    }

    QTcpSocket socket;
    socket.connectToHost(addr, receiver.getPort());
    if (!socket.waitForConnected(TIMEOUT)) {
        qWarning() << "CommunicationModule:" << socket.errorString();
        return false;
    }

    MessageConverter converter;
    QString xml = converter.toXmlString(message);
    if(sendData(&socket, xml.toUtf8()) == -1) {
        qWarning() << "CommunicationModule: Failed to send message: " << message << "to" << receiver;
        return false;
    }

    socket.disconnectFromHost();
    if(!socket.waitForDisconnected(TIMEOUT)) {
        qWarning() << "CommunicationModule: Error on disconnecting: " << socket.errorString();
        return false;
    }
    return true;
}

qint64 CommunicationModule::sendData(QTcpSocket *socket, const QByteArray &message) {
    QByteArray data;
    QDataStream out(&data, QIODevice::WriteOnly);
    out.setVersion(dataStreamVersion);
    out << MessageHeader(0);

    out << message;
    out.device()->seek(0);
    out << MessageHeader(data.size() - sizeof(MessageHeader));

    return socket->write(data); // Returns the number of bytes that were written, or -1 if an error occurred
}

void CommunicationModule::stopListening() {
    if(tcpServer->isListening())
        tcpServer->close();
}
