#ifndef RSO_MESSAGECONVERTER_H
#define RSO_MESSAGECONVERTER_H

#include <QString>
#include "Message.h"

class MessageConverter {
public:
    QString toXmlString(const Message& message) const;
    Message fromXmlString(const QString& xml) const;
};

#endif //RSO_MESSAGECONVERTER_H
