#ifndef RSO_SECTION_H
#define RSO_SECTION_H

#include "Host.h"

class Section {
public:
    Section(const std::string& name_) : name(name_) {}

    class Send {
    public:
        std::list<Host> hosts;

        bool addHost(const Host& host) {
            auto it = std::find(hosts.begin(), hosts.end(), host);
            if(it == hosts.end()) {
                hosts.push_back(host);
                return true;
            }
            return false;
        }
        bool removeHost(const Host& host) {
            const int initialSize = static_cast<const int>(hosts.size());
            hosts.remove(host);
            return initialSize != hosts.size();
        }
        bool removeAllHosts() {
            const int initialSize = static_cast<const int>(hosts.size());
            hosts.clear();
            return initialSize != hosts.size();
        }
    };

    // TODO: should be private
    std::string name;
    Host listen;
    Send send;
};

QDebug operator<<(QDebug qdb, const Section& s);

#endif //RSO_SECTION_H
