#include <QtCore/qfile.h>
#include <QtNetwork/qhostaddress.h>
#include "ConfigurationLoader.h"
#include "Configuration.h"

bool ConfigurationLoader::loadConfiguration(const QString &fileName, Configuration &config) {
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly)) {
        qWarning() << "ConfigurationLoader: Failed to open file " << fileName;
        return false;
    }
    bool rv = read(&file, config);
    if(file.isOpen())
        file.close();
    if(rv) {
        qDebug() << "ConfigurationLoader: Successfully loaded configuration: " << fileName;
    } else {
        qWarning() << "ConfigurationLoader: Error while loading configuration: " << fileName
            << "error: " << errorString();
    }
    return rv;
}

bool ConfigurationLoader::read(QIODevice *device, Configuration &config) {
    xml.setDevice(device);
    if (xml.readNextStartElement()) {
        if (xml.name() == "configuration") {
            readSections(config);
        }
        else xml.raiseError(QObject::tr("No main tag: configuration"));
    }
    return !xml.error();
}

void ConfigurationLoader::readSections(Configuration &configuration) {
    while (xml.readNextStartElement()) {
        if (xml.name() == "section")
            readSection(configuration);
        else {
            qWarning() << "ConfigurationLoader: readSections: Unknown tag: " << xml.name();
            xml.skipCurrentElement();
        }
    }
}

void ConfigurationLoader::readSection(Configuration &configuration) {
    QString name = xml.attributes().value("name").toString();
    Section section(name.toStdString());

    while(xml.readNextStartElement()) {
        if(xml.name() == "listen") {
            Host host;
            readHost(host);
            section.listen = host;
        }
        else if(xml.name() == "send") {
            readHosts(section);
        }
        else {
            qWarning() << "ConfigurationLoader: readSection: Unknown tag: " << xml.name();
            xml.skipCurrentElement();
        }
    }
    configuration.addSection(section);
}

void ConfigurationLoader::readHosts(Section &section) {
    while(xml.readNextStartElement()){
        if(xml.name() == "host") {
            Host host;
            readHost(host);
            section.send.addHost(host);
        }
        else {
            qWarning() << "ConfigurationLoader: readHosts: Unknown tag: " << xml.name();
            xml.skipCurrentElement();
        }
    }
}

void ConfigurationLoader::readHost(Host &host) {
    QString address = xml.attributes().value("address").toString();
    checkIPAddress(address);

    QStringRef port = xml.attributes().value("port");
    bool ok = false;
    int val = port.toInt(&ok);

    if(ok) host.setPort(static_cast<uint16_t>(val));
    else xml.raiseError("Invalid port number: " + port.toString());
    host.setAddress(address.toStdString());

    xml.skipCurrentElement();
}

QString ConfigurationLoader::errorString() const {
    return QObject::tr("%1\nLine %2, column %3")
            .arg(xml.errorString())
            .arg(xml.lineNumber())
            .arg(xml.columnNumber());
}

void ConfigurationLoader::checkIPAddress(const QString &addressString) {
    bool ok = addressString == "any" || addressString == "localhost";
    if(!ok) {
        QHostAddress address(addressString);
        if(QAbstractSocket::IPv4Protocol == address.protocol()
           || QAbstractSocket::IPv6Protocol == address.protocol())
            ok = true;
    }
    if(!ok) xml.raiseError("Invalid address: " + addressString);
}
