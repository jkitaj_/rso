# 1. Wymaga: #
    * C++11
    * Qt wersja 5+
    * GoogleTest (sam pobiera)
    * ...

# 2. Konfiguracja portów i adresów:
W folderach client, server i node są pliki konfiguracyjne: clientConfig.xml, serverConfig.xml, nodeConfig.xml.
Przykładowy dla serwera:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<configuration>
    <section name="client">     // sekcja dotyczaca komunikacji z klientami
        <listen address="any" port="3333"/>     // serwer slucha na wszystkich adresach, na porcie 3333
        <send>
            <host address="127.0.0.1" port="2222"/>   // wysyla do klienta na ten adres i port
        </send>
    </section>
    <section name="server">     // sekcja dotyczaca komunikacji z innymi serwerami
            <listen address="any" port="6666"/> // serwer slucha na wszystkich adresach, na porcie 6666
            <send>
                <host address="192.168.0.14" port="6666"/>  // lista serwerow z adresami i portami na jakie im wysylac
                <host address="192.168.0.15" port="6666"/>
            </send>
        </section>
    <section name="node">       // sekcja dotyczaca komunikacji z wezlami
        <listen address="any" port="5555"/>     // serwer slucha na wszystkich adresach, na porcie 5555
        <send>
        </send>
    </section>
</configuration>
```

# 3. Jak korzystać z servera: #
Uruchomienie: 

```
#!bash
$ server -m
```


# 4. Jak uruchamiać aplikację korzystając z Dockera: #

Na komputerze na którym odpalamy kontener z obrazem należy wykonać kolejno komendy:
```
#!bash
export DISPLAY=:0
xhost +
```

Pobranie obrazu: 

```
#!bash
$ sudo docker pull rsoweiti/rso
```

Do uruchamiania używamy skryptu start.sh z katalogu głównego.
Uruchamianie klienta:

```
#!bash
$ ./start.sh -c
```
Uruchamianie serwera:

```
#!bash
$ ./start.sh -s
```
Uruchamianie węzła:

```
#!bash
$ sudo ./start.sh -n
```

Pliki konfiguracyjne dla aplikacji należy umiescić na swoim komputerze w folderze /tmp/rso_config
Wymagane pliki to:

    * clientConfig.xml - konfiguracja dla klienta
    * serverConfig.xml - konfiguracja dla serwera
    * nodeConfig.xml - konfiguracja dla węzła
    * ip.txt - adres (lub adresy) ip bazy danych
    * cassandra.yaml - plik z konfiguracją dla bazy danych