FROM ubuntu:14.04
MAINTAINER Jakub Kitaj <jakub.kitaj@gmail.com>
RUN apt-get update 
RUN apt-get install -y build-essential 
RUN apt-get install -y cmake 
RUN apt-get install -y qt5-default 
RUN apt-get install -y openssl 
RUN apt-get install -y libssl-dev
RUN apt-get install -y x11-apps
RUN apt-get install -y wget
RUN apt-get install -y vim
RUN apt-get install -y python
RUN apt-get install -y xmlstarlet

#Install Java 8
RUN mkdir /opt/jdk
RUN cd /opt
RUN wget --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u45-b14/jdk-8u45-linux-x64.tar.gz
RUN tar -zxf jdk-8u45-linux-x64.tar.gz -C /opt/jdk
RUN update-alternatives --install /usr/bin/java java /opt/jdk/jdk1.8.0_45/bin/java 100
RUN update-alternatives --install /usr/bin/javac javac /opt/jdk/jdk1.8.0_45/bin/javac 100

#Get and install Cassandra DB
RUN wget http://ftp.ps.pl/pub/apache/cassandra/2.1.5/apache-cassandra-2.1.5-bin.tar.gz
RUN mkdir /cassandra
RUN tar -zxf apache-cassandra-2.1.5-bin.tar.gz -C /cassandra

#Get and install cassandra driver
ADD http://downloads.datastax.com/cpp-driver/ubuntu/14.04/libuv_1.4.2-1_amd64.deb /liby1
ADD http://downloads.datastax.com/cpp-driver/ubuntu/14.04/libuv-dev_1.4.2-1_amd64.deb /liby2
ADD http://downloads.datastax.com/cpp-driver/ubuntu/14.04/cassandra-cpp-driver_2.0.0-1_amd64.deb /liby3
ADD http://downloads.datastax.com/cpp-driver/ubuntu/14.04/cassandra-cpp-driver-dev_2.0.0-1_amd64.deb /liby4
RUN dpkg -i /liby1
RUN dpkg -i /liby2
RUN dpkg -i /liby3
RUN dpkg -i /liby4

#Copy code from repository to /rso
ADD / /rso
WORKDIR /rso

#Compile and run application
RUN mkdir /rso/build
RUN cmake -H/rso/ -B/rso/build
RUN cd /rso/build/ && make
ENV DISPLAY :0
