#ifndef _DATABASE_CPP_
#define _DATABASE_CPP_

#include "cassandra.h"
#include "person.h"
#include <vector>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>

class DataBase{
	private:
		CassCluster* cluster;
		CassSession* session;
		CassFuture* close_future;
		CassFuture* future;
		void print_error(CassFuture* future);
		CassError connect_session( );
		CassCluster* create_cluster();
		CassError execute_query( const char* query);
	public:
		DataBase();
		~DataBase();
		CassError createBase();
		CassError insert_into_person( const Person* person);
		CassError select_from_person(std::vector<Person*> &person);
		CassError select_from_person(const char* query, std::vector<Person*> &person);
		
};
DataBase::DataBase(){
	this->cluster= create_cluster();
	this->session= cass_session_new();
	this->close_future = NULL;

	if (connect_session() != CASS_OK) {
	    cass_cluster_free(this->cluster);
	    cass_session_free(this->session);
	    
	}
}
DataBase::~DataBase(){
	this->close_future = cass_session_close(this->session);
	cass_future_wait(this->close_future);
	cass_future_free(this->close_future);
	cass_cluster_free(this->cluster);
	cass_session_free(this->session);
}

void DataBase::print_error(CassFuture* future) {
  const char* message;
  size_t message_length;
  cass_future_error_message(future, &message, &message_length);
  fprintf(stderr, "Error: %.*s\n", (int)message_length, message);
}

CassCluster* DataBase::create_cluster() {
  CassCluster* cluster = cass_cluster_new();
  std::string line;
  std::ifstream namefile("ip.txt");
	 if (namefile.is_open())
	  {
	     getline (namefile,line);
	  }
  cass_cluster_set_contact_points(cluster, line.c_str());
  return cluster;
}

CassError DataBase::connect_session() {
  CassError rc = CASS_OK;
  this->future = cass_session_connect(this->session, this->cluster);

  cass_future_wait(this->future);
  rc = cass_future_error_code(this->future);
  if (rc != CASS_OK) {
    print_error(this->future);
  }
  cass_future_free(this->future);

  return rc;
}

CassError DataBase::execute_query( const char* query) {
  CassError rc = CASS_OK;
  this->future = NULL;
  CassStatement* statement = cass_statement_new(query, 0);

  future = cass_session_execute(this->session,statement);
  cass_future_wait(this->future);

  rc = cass_future_error_code(this->future);
  if (rc != CASS_OK) {
    print_error(this->future);
  }

  cass_future_free(this->future);
  cass_statement_free(statement);

  return rc;
}

CassError DataBase::createBase(){
  
  /*Tworzenie klucza*/
  const char* query = "CREATE KEYSPACE mykeyspace WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '2'};";
  this->future = NULL;
  CassError rc = CASS_OK;
  CassStatement* statement = NULL;
  statement = cass_statement_new(query, 0);

  this->future = cass_session_execute(this->session,statement);
  cass_future_wait(this->future);

  rc = cass_future_error_code(this->future);
  if (rc != CASS_OK) {
    print_error(this->future);	
  }else{
  	std::cout<<"Udało się utworzyć KEYSPACE"<<std::endl;
  }
 
  /*Tworzenie tabeli platnikow*/

query = "CREATE TABLE mykeyspace.taxpayer (id int,fname text, lname text, sex text, province text, ernings_2010 double,  ernings_2011 double, ernings_2012 double, ernings_2013 double, ernings_2014 double, PRIMARY KEY (id, fname, lname, sex, province, ernings_2010, ernings_2011, ernings_2012, ernings_2013, ernings_2014) )";
  this->future = NULL;
  rc = CASS_OK;
  statement = NULL;
  statement = cass_statement_new(query, 0);

  this->future = cass_session_execute(this->session,statement);
  cass_future_wait(this->future);

  rc = cass_future_error_code(this->future);
  if (rc != CASS_OK) {
    print_error(this->future);	
  }else{
  	std::cout<<"Udało się utworzyć tabele taxpayer"<<std::endl;
  }

	/*Tworzenie indeksow dla platnikow*/

query = "CREATE INDEX taxpayer_province_idx ON mykeyspace.taxpayer (province);";
  this->future = NULL;
  rc = CASS_OK;
  statement = NULL;
  statement = cass_statement_new(query, 0);

  this->future = cass_session_execute(this->session,statement);
  cass_future_wait(this->future);

  rc = cass_future_error_code(this->future);
  if (rc != CASS_OK) {
    print_error(this->future);	
  }else{
  	std::cout<<"Utworzono index na columnie province"<<std::endl;
  }
/*Tworzenie tabeli z loginami hostow*/
query = "CREATE TABLE mykeyspace.hosts (login text, password text, PRIMARY KEY (login, password))";
  this->future = NULL;
  rc = CASS_OK;
  statement = NULL;
  statement = cass_statement_new(query, 0);

  this->future = cass_session_execute(this->session,statement);
  cass_future_wait(this->future);

  rc = cass_future_error_code(this->future);
  if (rc != CASS_OK) {
    print_error(this->future);	
  }else{
  	std::cout<<"Utworzono tabele hosts"<<std::endl;
  }
/*Dodanie danych do tabeli z loginami hostow*/
  query = "INSERT INTO mykeyspace.hosts (login,  password) VALUES  ('admin', 'admin');";
  this->future = NULL;
  rc = CASS_OK;
  statement = NULL;
  statement = cass_statement_new(query, 0);

  this->future = cass_session_execute(this->session,statement);
  cass_future_wait(this->future);

  rc = cass_future_error_code(this->future);
  if (rc != CASS_OK) {
    print_error(this->future);	
  }else{
  	std::cout<<"Dodano hosta admin admin"<<std::endl;
  }

/*Tworzenie tabeli zadan*/
query = "CREATE TABLE mykeyspace.task (state int, updated timestamp, function text, reply text, info text, c_id int, c_ip text, c_p int, c_login text, c_pass text, s_ip text, n_ip text, PRIMARY KEY (function,  c_id, c_ip, c_login, c_p) )";
  this->future = NULL;
  rc = CASS_OK;
  statement = NULL;
  statement = cass_statement_new(query, 0);

  this->future = cass_session_execute(this->session,statement);
  cass_future_wait(this->future);

  rc = cass_future_error_code(this->future);
  if (rc != CASS_OK) {
    print_error(this->future);	
  }else{
  	std::cout<<"Utworzono tabele task"<<std::endl;
  }




/*Tworzenie przykladowego zadania */
query = "INSERT INTO mykeyspace.task (state, updated, function, reply, info, c_id, c_ip, c_p, c_login, c_pass, s_ip, n_ip) VALUES  (30,dateof(now()), 'zadanie', 'Test', '', 0, '192.168.0.102', 2222, 'admin', 'admin', 'o', 'tak');";

  this->future = NULL;
  rc = CASS_OK;
  statement = NULL;
  statement = cass_statement_new(query, 0);

  this->future = cass_session_execute(this->session,statement);
  cass_future_wait(this->future);

  rc = cass_future_error_code(this->future);
  if (rc != CASS_OK) {
    print_error(this->future);	
  }else{
  	std::cout<<"Utworzono testowe zadanie w bazie danych"<<std::endl;
  }
	
/*Tworzenie indeksow do zadania */ 
query = "CREATE INDEX ON mykeyspace.task (state); ";

  this->future = NULL;
  rc = CASS_OK;
  statement = NULL;
  statement = cass_statement_new(query, 0);

  this->future = cass_session_execute(this->session,statement);
  cass_future_wait(this->future);

  rc = cass_future_error_code(this->future);
  if (rc != CASS_OK) {
    print_error(this->future);
  }else{
  	std::cout<<"Utworzono index na columnie state"<<std::endl;
  }
query = "CREATE INDEX ON mykeyspace.task (updated);";

  this->future = NULL;
  rc = CASS_OK;
  statement = NULL;
  statement = cass_statement_new(query, 0);

  this->future = cass_session_execute(this->session,statement);
  cass_future_wait(this->future);

  rc = cass_future_error_code(this->future);
  if (rc != CASS_OK) {
    print_error(this->future);	
  }else{
  	std::cout<<"Utworzono index na columnie timestamp"<<std::endl;
  }
/*Tworzenie tabeli z timestampem na urzytek nodów. */ 
	query = "CREATE TABLE mykeyspace.time (nr int PRIMARY KEY, time timestamp);"; 
	this->future = NULL;
	  rc = CASS_OK;
	  statement = NULL;
	  statement = cass_statement_new(query, 0);

	  this->future = cass_session_execute(this->session,statement);
	  cass_future_wait(this->future);

	  rc = cass_future_error_code(this->future);
	  if (rc != CASS_OK) {
		print_error(this->future);	
	  } else{
	  		std::cout<<"Utworzono tabele time"<<std::endl;
	  }
/*Tworzenie recordu timestampu*/	  
	  query = "INSERT INTO mykeyspace.time (nr, time) VALUES (0 ,dateof(now()));";
	  this->future = NULL;
	  rc = CASS_OK;
	  statement = NULL;
	  statement = cass_statement_new(query, 0);

	  this->future = cass_session_execute(this->session,statement);
	  cass_future_wait(this->future);

	  rc = cass_future_error_code(this->future);
	  if (rc != CASS_OK) {
		print_error(this->future);	
	  }
	  
	  /*Tworzenie tabeli serwerów*/	  
	  query = "Create Table mykeyspace.servers (id int PRIMARY KEY, ip text, port int );";
	  this->future = NULL;
	  rc = CASS_OK;
	  statement = NULL;
	  statement = cass_statement_new(query, 0);

	  this->future = cass_session_execute(this->session,statement);
	  cass_future_wait(this->future);

	  rc = cass_future_error_code(this->future);
	  if (rc != CASS_OK) {
		print_error(this->future);	
	  }
}
CassError DataBase::insert_into_person( const Person* person) {
  CassError rc = CASS_OK;
  CassStatement* statement = NULL;
  this->future = NULL;
  const char* query = "INSERT INTO mykeyspace.taxpayer (id,  fname, lname, sex, province, ernings_2010, ernings_2011, ernings_2012, ernings_2013, ernings_2014) VALUES  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

  statement = cass_statement_new(query, 10);

	cass_statement_bind_int32(statement, 0, person->id);
 	cass_statement_bind_string(statement, 1, person->fname);
	cass_statement_bind_string(statement, 2, person->lname);
	cass_statement_bind_string(statement, 3, person->sex);
	cass_statement_bind_string(statement, 4, person->province);
	cass_statement_bind_double(statement, 5, person->ernings_2010);
	cass_statement_bind_double(statement, 6, person->ernings_2011);
	cass_statement_bind_double(statement, 7, person->ernings_2012);
	cass_statement_bind_double(statement, 8, person->ernings_2013);
	cass_statement_bind_double(statement, 9, person->ernings_2014);

  this->future = cass_session_execute(this->session,statement);
  cass_future_wait(this->future);

  rc = cass_future_error_code(this->future);
  if (rc != CASS_OK) {
    print_error(this->future);	
  }

  cass_future_free(this->future);
  cass_statement_free(statement);

  return rc;
}

CassError DataBase::select_from_person(std::vector<Person*> &person) {
  CassError rc = CASS_OK;
  CassStatement* statement = NULL;
  const char* key ="opolskie";
  this->future = NULL;
  const char* query = "SELECT * FROM mykeyspace.taxpayer WHERE province=? and ernings_2013<27593 LIMIT 5 ALLOW FILTERING ;";

  statement = cass_statement_new(query, 1);


  //cass_statement_bind_string(statement, 0, key);

  future = cass_session_execute(this->session, statement);
  cass_future_wait(this->future);

  rc = cass_future_error_code(this->future);
  if (rc != CASS_OK) {
	print_error(this->future);
  } else {
    const CassResult* result = cass_future_get_result(this->future);
    CassIterator* iterator = cass_iterator_from_result(result);

	int i=0;
   while(cass_iterator_next(iterator)) {
      person.push_back(new Person());
      const CassRow* row = cass_iterator_get_row(iterator);
	cass_value_get_int32(cass_row_get_column(row, 0), &person[i]->id);
	//cass_value_get_string(cass_row_get_column(row, 1), fname);
	cass_value_get_double(cass_row_get_column(row, 5), &person[i]->ernings_2010);
	cass_value_get_double(cass_row_get_column(row, 6), &person[i]->ernings_2011);
	cass_value_get_double(cass_row_get_column(row, 7), &person[i]->ernings_2012);
	cass_value_get_double(cass_row_get_column(row, 8), &person[i]->ernings_2013);
	cass_value_get_double(cass_row_get_column(row, 9), &person[i]->ernings_2014);
     	i++;
  //    
    /*  cass_value_get_string(cass_row_get_column(row, 2), &person->lname);
      cass_value_get_string(cass_row_get_column(row, 3), &person->sex);
      cass_value_get_string(cass_row_get_column(row, 4), &person->province);
     	cass_value_get_double(cass_row_get_column(row, 5), &person->ernings_2010);
	cass_value_get_double(cass_row_get_column(row, 6), &person->ernings_2011);
	cass_value_get_double(cass_row_get_column(row, 7), &person->ernings_2012);
	cass_value_get_double(cass_row_get_column(row, 8), &person->ernings_2013);
	cass_value_get_double(cass_row_get_column(row, 9), &person->ernings_2014);*/
    }

    cass_result_free(result);
    cass_iterator_free(iterator);
  }

  cass_future_free(this->future);
  cass_statement_free(statement);

  return rc;
}

CassError DataBase::select_from_person(const char* query ,std::vector<Person*> &person) {
  CassError rc = CASS_OK;
  CassStatement* statement = NULL;
  const char* key ="*";
  this->future = NULL;
  

  statement = cass_statement_new(query, 0);


  //cass_statement_bind_string(statement, 0, key);

  future = cass_session_execute(this->session, statement);
  cass_future_wait(this->future);

  rc = cass_future_error_code(this->future);
  if (rc != CASS_OK) {
	print_error(this->future);
  } else {
    const CassResult* result = cass_future_get_result(this->future);
    CassIterator* iterator = cass_iterator_from_result(result);

	int i=0;
   while(cass_iterator_next(iterator)) {
      person.push_back(new Person());
      const CassRow* row = cass_iterator_get_row(iterator);
	cass_value_get_int32(cass_row_get_column(row, 0), &person[i]->id);
	
	cass_value_get_double(cass_row_get_column(row, 5), &person[i]->ernings_2010);
	cass_value_get_double(cass_row_get_column(row, 6), &person[i]->ernings_2011);
	cass_value_get_double(cass_row_get_column(row, 7), &person[i]->ernings_2012);
	cass_value_get_double(cass_row_get_column(row, 8), &person[i]->ernings_2013);
	cass_value_get_double(cass_row_get_column(row, 9), &person[i]->ernings_2014);
     	i++;
  }
    cass_result_free(result);
    cass_iterator_free(iterator);
  }

  cass_future_free(this->future);
  cass_statement_free(statement);

  return rc;
}


#endif

