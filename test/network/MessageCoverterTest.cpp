#include <gtest/gtest.h>
#include "MessageConverter.h"

// TODO: more tests, different messages etc.

TEST(TwoWayConversionTest, MessageToMessage) {
    const int type = 1;
    const QString text("Polskie znaki: ąęśćńżźół 123");
    const bool ok = false;  // TODO: check also true

    Message m(type, text, ok);
    MessageConverter converter;

    QString xml = converter.toXmlString(m);
    Message m2 = converter.fromXmlString(xml);

    ASSERT_TRUE(m == m2);
    ASSERT_FALSE(m != m2);
}