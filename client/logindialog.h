#ifndef RSO_LOGINDIALOG_H
#define RSO_LOGINDIALOG_H

#include <QDialog>
#include <QLabel>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QGridLayout>
#include <QStringList>
#include <QDebug>

class LoginDialog : public QDialog
{

Q_OBJECT

private:
    QLabel* labelUsername;
    QLabel* labelPassword;
    QLineEdit* editUsername;
    QLineEdit* editPassword;
    QDialogButtonBox* buttons;

    void setUpGUI();

public:
    explicit LoginDialog(QWidget* parent = 0);

signals:
    void sendCredentials( QString& username, QString& password);

public slots:
    void slotSendCredentials();

};

#endif //RSO_LOGINDIALOG_H
