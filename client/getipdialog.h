#ifndef RSO_GETIPDIALOG_H
#define RSO_GETIPDIALOG_H

#include <QDialog>
#include <QLabel>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QGridLayout>
#include <QStringList>
#include <QDebug>

class GetIpDialog : public QDialog
{

Q_OBJECT

private:
    QLabel* labelIP;
    QLineEdit* editIP;
    QDialogButtonBox* buttons;

    void setUpGUI();

public:
    explicit GetIpDialog(QWidget* parent = 0);

signals:
    void sendIp( QString& ip );

public slots:
    void slotSendIp();

};

#endif //RSO_GETIPDIALOG_H
