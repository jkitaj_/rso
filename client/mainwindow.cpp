#include "mainwindow.h"
#include "logindialog.h"
#include "getipdialog.h"
#include "networkConfig.h"
#include "ClientCommunicationModule.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    QWidget *mainWidget = new QWidget(this);
    setCentralWidget(mainWidget);
    setWindowTitle(tr("RSO Client Application"));
    setMinimumWidth(500);
    setMinimumHeight(250);

    showLoginDialog();
    showGetIpDialog();

    queryID=0;

    m_mainLayout = new QVBoxLayout;
    {
        QHBoxLayout *sendHBoxLayout = new QHBoxLayout();
        {
            m_sendMessagePushButton = new QPushButton("Send");
            sendHBoxLayout->addWidget(m_sendMessagePushButton);

            m_queryLineEdit = new QLineEdit("Query");
            sendHBoxLayout->addWidget(m_queryLineEdit);
        }
        m_mainLayout->addLayout(sendHBoxLayout);

        QVBoxLayout *receiveVBoxLayout = new QVBoxLayout;
        {
            m_sentLabel = new QLabel("Messages sent");
            receiveVBoxLayout->addWidget(m_sentLabel);

            m_sentMessageTableWidget = createMessageTable();
            receiveVBoxLayout->addWidget(m_sentMessageTableWidget);

            m_receivedLabel = new QLabel("Messages received");
            receiveVBoxLayout->addWidget(m_receivedLabel);

            m_receivedMessageTableWidget = createMessageTable();
            receiveVBoxLayout->addWidget(m_receivedMessageTableWidget);
        }
        m_mainLayout->addLayout(receiveVBoxLayout);
    }
    mainWidget->setLayout(m_mainLayout);

    m_comModule = new ClientCommunicationModule(CLIENT_CONFIG_FILE_NAME, this);
    connect(m_sendMessagePushButton, SIGNAL(clicked()), this, SLOT(onSendButtonClicked()));
    connect(m_comModule, SIGNAL(sentMessage(Message)), this, SLOT(onSentMessage(Message)));
    connect(m_comModule, SIGNAL(receivedMessage(Message)), this, SLOT(onReceiveMessage(Message)));
    connect(m_comModule, SIGNAL(noConnection()), this, SLOT(onNoConnection()));

    m_comModule->setDefaultHost(serverIP.toStdString());
    m_comModule->listenToConnectionsFrom(m_comModule->SERVER_SECTION);
}

MainWindow::~MainWindow()
{
}

QTableWidget* MainWindow::createMessageTable()
{
    QTableWidget *messageTable = new QTableWidget();

    messageTable->setRowCount(0);
    messageTable->setColumnCount(4);

    messageTable->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    messageTable->setHorizontalHeaderLabels(QString("ID;Type;Query;Response").split(";"));

    messageTable->verticalHeader()->setVisible(false);

    return messageTable;
}

void MainWindow::appendMessage(const Message &message, QTableWidget* tableWidget)
{
    tableWidget->insertRow( tableWidget->rowCount() );
    int rowCount = tableWidget->rowCount();

    QTableWidgetItem* tableItem = new QTableWidgetItem();

    tableItem->setText(QString::fromStdString(std::to_string(message.getID())));
    tableWidget->setItem(rowCount-1, 0, tableItem);

    tableItem = new QTableWidgetItem();
    tableItem->setText(message.getMessageType());
    tableWidget->setItem(rowCount-1, 1, tableItem);

    tableItem = new QTableWidgetItem();
    tableItem->setText(QString::fromStdString(message.getQuery()));
    tableWidget->setItem(rowCount-1, 2, tableItem);

    tableItem = new QTableWidgetItem();
    tableItem->setText(QString::fromStdString(message.getResponse()));
    tableWidget->setItem(rowCount-1, 3, tableItem);

    tableWidget->resizeRowsToContents();
    tableWidget->resizeColumnsToContents();

    tableWidget->scrollToBottom();
}

void MainWindow::showGetIpDialog()
{
    GetIpDialog* getIpDialog = new GetIpDialog( this );
    connect( getIpDialog, SIGNAL (sendIp(QString&)),
             this, SLOT (onIpSent(QString&)));
    getIpDialog->exec();
}

void MainWindow::onIpSent(QString& ip)
{
    serverIP = ip;
}

void MainWindow::showLoginDialog()
{
    LoginDialog* loginDialog = new LoginDialog( this );
    connect( loginDialog, SIGNAL (sendCredentials(QString&,QString&)),
             this, SLOT (onCredentialsSent(QString&,QString&)));
    loginDialog->exec();
}

void MainWindow::onCredentialsSent(QString& login, QString& password)
{
    userLogin = login;
    userPassword = password;
}

void MainWindow::onSendButtonClicked()
{
    string login = userLogin.toStdString();
    string password = userPassword.toStdString();
    string query = m_queryLineEdit->text().toStdString();

    m_comModule->sendQuery(login, password, query, queryID);
    ++queryID;
}

void MainWindow::onSentMessage(Message message)
{
    appendMessage(message, m_sentMessageTableWidget);
}

void MainWindow::onReceiveMessage(Message message)
{
    appendMessage(message, m_receivedMessageTableWidget);
}

void MainWindow::onNoConnection()
{
    QMessageBox msgBox;
    msgBox.critical(0, "Error", "Failed to send message. Could not connect to server.");
}


