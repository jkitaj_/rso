#include "ClientCommunicationModule.h"
#include "networkConfig.h"

#include <thread>

ClientCommunicationModule::ClientCommunicationModule(const std::string& configFileName, QObject *parent)
        : CommunicationModule(configFileName, parent)
{
//    srand(time(0));
    std::srand(std::time(0));
}

void ClientCommunicationModule::onReceive(Message& message) {
    // message to albo wynik zapytania, albo lista serwerow

    //TODO stuff after receiving message

    if(message.getResponse()=="Wrong Pass") {
        qWarning() << "ERROR: Wrong credentials.";
    }

    std::list<Host> servers = message.getServers();

    if(servers.size() > 0) {
        qDebug() << "ClientCommunicationModule: Received new list of " << servers.size() << " servers.";

        forgetAllServers();
        for (Host h: servers) {
            h.setPort(SERVER_LISTEN_PORT_FOR_CLIENT);
            addServer(h);
        }
    }

    messages.push_back(message);

    emit receivedMessage(message);
}

void ClientCommunicationModule::startModule()
{
    listenToConnectionsFrom(SERVER_SECTION);
}

void ClientCommunicationModule::onMessageSent(const Host &receiver, const Message &message) {
    // successfully sent message
    qDebug() << "ClientCommunicationModule: Message was successfully sent to server.";

    messages.push_back(message);

    emit sentMessage(message);
}

void ClientCommunicationModule::onFailedToSendMessage(const Host &receiver, const Message &message) {
    // failed to send, server might be dead -> use different server
    Message msg = message;

    qDebug() << "ClientCommunicationModule: Failed to send message " << message << " to server"
             << receiver;
    qDebug() <<"ClientCommunicationModule: Trying to connect to another server.";

    msg.addServer(receiver);    // save faulty server in message - some other server should check it
    std::list<Host> faultyServers = msg.getServers();

    std::list<Host> knownServers;
    {
        knownServers = getServers();
    }
    for(Host& fs : faultyServers) // remove servers which are faulty - we already tried to send to them
        knownServers.remove(fs);

    if(knownServers.empty()) {
        qWarning() << "ClientCommunicationModule: Cannot send message" << message
        << "tried to send to all known servers but failed!";
        qWarning() << "ERROR: Could not connect to any server. Message was not sent." << endl;

        emit noConnection();
    } else {
        Host randomServer = getRandomServer(knownServers);
        send(randomServer, msg);
    }
}

std::list<Host> ClientCommunicationModule::getServers() {
    return getConfigurationManager()->getHostsFromSection(SERVER_SECTION);
}

Host ClientCommunicationModule::getRandomServer(const std::list<Host> &servers) {
    int idx = static_cast<int>(std::rand() % servers.size());
    return *std::next(servers.begin(), idx);
}

uint ClientCommunicationModule::getAndIncrementMessageID() {
    return messageID++;
}

void ClientCommunicationModule::addServer(const Host &server) {
    getConfigurationManager()->addHostToSection(server, SERVER_SECTION);
    qDebug() << "ClientCommunicationModule: Added server " << server << " to server section.";
}

void ClientCommunicationModule::forgetAllServers() {
    getConfigurationManager()->removeAllHostsFromSection(SERVER_SECTION);
}

void ClientCommunicationModule::sendMessage(Message& message)
{
    std::list<Host> servers = getServers(); // get all servers that are known
    if(!servers.empty()) {
        Host host = getRandomServer(servers);   // get random server from the list from configuration
        send(host, message);
    }
}

void ClientCommunicationModule::sendQuery(string login, string password, string query, int id) {

    Message message(Message::Type::CLIENT_REQUEST, "Query");
    message.setLogin(login);
    message.setPassword(password);
    message.setQuery(query);
    message.setID(id);
    message.setClientIP(getListenAddress());
    message.setClientPort(getListenPort());
    message.setSenderIP(getListenAddress());
    message.setSenderPort(getListenPort());

    sendMessage(message);

    message.print();
}

void ClientCommunicationModule::setDefaultHost(string ip) {
    forgetAllServers();

    Host host(ip, SERVER_LISTEN_PORT_FOR_CLIENT);
    addServer(host);
}

