#include "getipdialog.h"

GetIpDialog::GetIpDialog(QWidget* parent) : QDialog(parent)
{
    setUpGUI();
    setWindowTitle( tr("Server IP") );
    setModal( true );
}

void GetIpDialog::setUpGUI(){
    // set up the layout
    QGridLayout* formGridLayout = new QGridLayout( this );

    editIP = new QLineEdit( this );

// initialize the labels
    labelIP = new QLabel( this );
    labelIP->setText( tr( "Please type server IP" ) );
    labelIP->setBuddy( editIP );


// initialize buttons
    buttons = new QDialogButtonBox( this );
    buttons->addButton( QDialogButtonBox::Ok );

    connect( buttons->button( QDialogButtonBox::Ok ),
             SIGNAL (clicked()),
             this,
             SLOT (slotSendIp()) );

// place components into the dialog
    formGridLayout->addWidget( labelIP, 0, 0 );
    formGridLayout->addWidget( editIP, 0, 1 );
    formGridLayout->addWidget( buttons, 1, 0, 1, 2 );

    setLayout( formGridLayout );

}

void GetIpDialog::slotSendIp(){
    QString IP = editIP->text();

    emit sendIp( IP );

// close this dialog
    close();
}
