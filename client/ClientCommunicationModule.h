#ifndef RSO_CLIENTCOMMUNICATIONMODULE_H
#define RSO_CLIENTCOMMUNICATIONMODULE_H

#include <QObject>
#include <iostream>
#include "CommunicationModule.h"

#define prompt cout<<"client> "

using namespace std;

class ClientCommunicationModule : public CommunicationModule {
    Q_OBJECT

public:
    ClientCommunicationModule(const std::string& configFileName, QObject *parent = 0);

    virtual void onReceive(Message& message);
    virtual void onMessageSent(const Host& receiver, const Message& message);
    virtual void onFailedToSendMessage(const Host& receiver, const Message& message);
    void startModule();
    void sendMessage(Message &message);
    void sendQuery(string, string, string, int);
    void setDefaultHost(string ip);

    const std::string SERVER_SECTION = "server";
    std::list<Host> getServers();
    std::list<Message> getMessages();
    std::list<Message> getUnknownMessages();
    uint getAndIncrementMessageID();

signals:
    void sentMessage(Message message);
    void receivedMessage(Message message);
    void noConnection();

private:
    uint messageID = 0;
    Host getRandomServer(const std::list<Host> &servers);
    void addServer(const Host& server);
    void forgetAllServers();
    std::list<Message> messages;
    std::list<Message> unknownMessages;

};

#endif //RSO_CLIENTCOMMUNICATIONMODULE_H
