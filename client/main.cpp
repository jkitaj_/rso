#include <QtCore/qcoreapplication.h>
#include <networkConfig.h>
#include "ClientCommunicationModule.h"
#include <iostream>

#define prompt cout<<"client> "

using namespace std;

int main(int argc, char *argv[]) {
    QCoreApplication a(argc, argv);

    string login, password, query;
    prompt << "Please type your login" << endl;
    prompt;
    cin >> login;
    prompt << "Please type your password" << endl;
    prompt;
    cin >> password;
    prompt << "Please type your query" << endl;
    prompt;
    cin >> query;

    ClientCommunicationModule module(CLIENT_CONFIG_FILE_NAME);
    module.startModule();
    module.sendQuery(login, password, query, 0);

    prompt << "Waiting for server response..." << endl;

    QObject::connect(&module, SIGNAL(Done()), a.instance(), SLOT(quit()));

    return a.exec();
}
