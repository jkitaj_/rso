#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets/QMainWindow>
#include <QPushButton>
#include <QVBoxLayout>
#include <QTextEdit>
#include <QLineEdit>
#include <QMessageBox>
#include <QTableWidget>
#include <QHeaderView>

namespace Ui {
class MainWindow;
}

class ClientCommunicationModule;
class Message;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QVBoxLayout *m_mainLayout;
    QPushButton *m_sendMessagePushButton;

    QLineEdit *m_queryLineEdit;

    QTableWidget *m_sentMessageTableWidget;
    QTableWidget *m_receivedMessageTableWidget;

    QTableWidget* createMessageTable();

    QLabel *m_sentLabel;
    QLabel *m_receivedLabel;

    ClientCommunicationModule *m_comModule;

    void showGetIpDialog();
    void showLoginDialog();

    void appendMessage(const Message &message, QTableWidget* tableWidget);

public slots:
    void onIpSent(QString& ip);
    void onCredentialsSent(QString& login, QString& password);
    void onSendButtonClicked();
    void onSentMessage(Message message);
    void onReceiveMessage(Message message);
    void onNoConnection();

private:
    QString serverIP;
    int 	queryID;
    QString userLogin;
    QString userPassword;
};

#endif // MAINWINDOW_H
