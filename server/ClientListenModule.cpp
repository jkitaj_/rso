#include "ClientListenModule.h"
#include "MessageRound.h"

#include <fstream>
#include <sstream>

#define consoleLog 0

#if consoleLog
    #define promptServerBeg std::cout<<"server-sc> "
    #define promptServer std::cout
#else
std::stringstream promptClient;
std::stringstream &promptClientBeg = promptClient;
#endif

ClientListenModule::~ClientListenModule()
{
    delete cmLogger;
}

ClientListenModule::ClientListenModule(ConfigManagerPtr manager, MessageRound *msgQueue, QObject *parent)
        : CommunicationModule(manager, parent),
          messageQueue(msgQueue)
{
    cmLogger = new CMLoggerWindow();
    cmLogger->show();
    cmLogger->setWindowTitle("Logger: server to client "
                             + QString::fromStdString(getMyInterface().getAddress())
                             + ":"
                             + QString::number(getMyInterface().getPort())
    );
}

void ClientListenModule::onReceive(Message& message_) {
    cmLogger->appednMessage(CMLoggerWindow::INCOMING, message_, getMyInterface());

#if consoleLog==0
    promptClient.str(std::string());
#endif

    Message message = message_;
    qDebug() << "ServerListenModule: I received message" << message;
    promptClientBeg << std::string(5, '=') << " onReceive " << std::string(5, '=') << std::endl;
    Host sender = message.getSender();
    int mType = message.getType();

    Host client(message.getClientIP(), message.getClientPort());
    addClient(client);  // add client to list in configuration

    message.setServerIP(getListenAddress());
    message.setServerPort(getListenPort());

    messageQueue->setMessage(message);

#if consoleLog==0
    cmLogger->setMessageInfo(promptClient.str());
#endif
}

std::list<Host> ClientListenModule::getServers() {
    return getConfigurationManager()->getHostsFromSection(SERVER_SECTION);
}

std::list<Host> ClientListenModule::getClients() {
    return getConfigurationManager()->getHostsFromSection(CLIENT_SECTION);
}

void ClientListenModule::addClient(const Host &host) {
    getConfigurationManager()->addHostToSection(host, CLIENT_SECTION);
}

//void ClientListenModule::sendToClient(const Message &message) {
//    Host client(message.getClientIP(), message.getClientPort());
//    addClient(client);  // remember client
//    send(client.getAddress(), client.getPort(), message);
//}

void ClientListenModule::onMessageSent(const Host &receiver, const Message &message) {
    // successfully sent message to client
    qDebug() << "ClientListenModule: Message to client was sent successfully.";
}

void ClientListenModule::onFailedToSendMessage(const Host &receiver, const Message &message) {
    // failed to send message -> client might be dead
}

bool ClientListenModule::sendToClientAndWait(Message &message) {
    if(!sendAndWait(Host(message.getClientIP(), message.getClientPort()), message)) {
        message.setState(Message::State::ERROR);
        message.setInfo("Failed to send");
        return false;
    }
    return true;
}

void ClientListenModule::listenToConnectionsFromClients() {
    listenToConnectionsFrom(CLIENT_SECTION);
}

void ClientListenModule::addServersList(Message &message) {
    std::list<Host> servers = getServers(); // all known servers
    message.appendServers(servers);

    qDebug() << "ClientListenModule: Servers list added to message.";
}

Host ClientListenModule::getMyInterface()
{
    return getConfigurationManager()->getCurrentConfiguration().getSectionByName(CLIENT_SECTION).listen;
}

void ClientListenModule::handledCaseLog(const int &type)
{
    Message MSG90;
    MSG90.setType(type);

    promptClientBeg << MSG90.getMessageType().toStdString() << std::endl;
}