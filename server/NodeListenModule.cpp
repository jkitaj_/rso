#include "NodeListenModule.h"
#include "MessageRound.h"
#include <iostream>
#include <fstream>
#include <sstream>

#include <database.h>

#include "cmloggerwindow.h"

#define consoleLog 0

#if consoleLog
    #define promptNodeBeg std::cout<<"server-sn> "
    #define promptNode std::cout
#else
std::stringstream promptNode;
std::stringstream &promptNodeBeg = promptNode;
#endif

NodeListenModule::~NodeListenModule()
{
    delete cmLogger;
}

NodeListenModule::NodeListenModule(ConfigManagerPtr manager, MessageRound *msgQueue, QObject *parent)
        : CommunicationModule(manager, parent),
          messageQueue(msgQueue)
{
    cmLogger = new CMLoggerWindow();
    cmLogger->show();
    cmLogger->setWindowTitle("Logger: server to node "
                             + QString::fromStdString(getMyInterface().getAddress())
                             + ":"
                             + QString::number(getMyInterface().getPort())
    );
}

void NodeListenModule::onReceive(Message& message_) {
    cmLogger->appednMessage(CMLoggerWindow::INCOMING, message_, getMyInterface());

#if consoleLog==0
    promptNode.str(std::string());
#endif

    Message message = message_;
    qDebug() << "NodeListenModule: I received message" << message;
    promptNodeBeg << std::string(5, '=') << " onReceive " << std::string(5, '=') << std::endl;
    Host sender = message.getSender();
    int mType = message.getType();

    switch (mType) {
        case Message::Type::NODE_GETDBADDRESS: {
            if (authenticate(message.getLogin(), message.getPassword())) {
                handledCaseLog(mType);

                std::string line;
                std::ifstream namefile("ip.txt");
                if (namefile.is_open()) {
                    getline(namefile, line);

                    message.setType(Message::Type::SERVER_NODERESPDBADDRESS);
                    message.setInfo(line);

                    Host sender = message.getSender();

                    message.setSender(getMyInterface());
                    send(sender, message);

                } else {
                    promptNodeBeg << "Cant open file with DataBase IP" << std::endl;
                }
            } else {
                promptNodeBeg << "Nie uwierzytelniony hahaha HOHOHO" << std::endl;

                message.setType(Message::Type::SERVER_NODERESPDBADDRESS);
                message.setInfo("Co zrobisz? Nic nie zrobisz!");

                Host sender = message.getSender();

                message.setSender(getMyInterface());
                send(sender, message);
            }
            break;
        }
        default: {
            std::list<Host> servers = message.getServers();
            messageQueue->setMessage(message);
            if (!servers.empty()) {
                emit receivedFaultyServerList(servers);
            }
            break;
        }
    }
#if consoleLog==0
    cmLogger->setMessageInfo(promptNode.str());
#endif
}

void NodeListenModule::listenToConnectionsFromNodes() {
    listenToConnectionsFrom(NODE_SECTION);
}

Host NodeListenModule::getMyInterface()
{
    return getConfigurationManager()->getCurrentConfiguration().getSectionByName(NODE_SECTION).listen;
}

void NodeListenModule::handledCaseLog(const int &type)
{
    Message MSG90;
    MSG90.setType(type);

    promptNodeBeg << MSG90.getMessageType().toStdString() << std::endl;
}

void NodeListenModule::unhandledCase(const Message &message)
{
    promptNodeBeg << std::endl;
    promptNodeBeg << std::string(20, '=') << " Unhandled case " << std::string(20, '=') << std::endl;
//    promptNodeBeg << message;
    promptNodeBeg << std::string(20, '=') << " End unhandled case " << std::string(20, '=') << std::endl;
    promptNodeBeg << std::endl;
}

bool NodeListenModule::authenticate(const std::string &login, const std::string &password) { // TODO: don't print password
    DataBase db;
    bool ok = db.checkHost(login, password);
    if(ok) promptNodeBeg << "NodeListenModule: Authentication successful: login: " << login << " password: " << password << std::endl;
    else promptNodeBeg << "NodeListenModule: Authentication failed: login: " << login << " password: " << password << std::endl;
    return ok;
}