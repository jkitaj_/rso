#include <QtWidgets/QApplication>
#include <networkConfig.h>
#include "NodeListenModule.h"
#include "ServerListenModule.h"
#include "ClientListenModule.h"
#include "MessageRound.h"
#include "ServerDBConnector.h"
#include "Responder.h"
#include "Credentials.h"
#include "CredentialsReader.h"

#include <QNetworkInterface>
#include <QHostInfo>
#include <QCommandLineParser>

#define prompt std::cout<<"server> "
typedef std::shared_ptr<ConfigurationManager> ConfigManagerPtr;

Host addListener() // TODO
{
    std::string ipAddress;
    prompt << "Please specify ip address for one of servers net" << std::endl;
    prompt;
    std::cin >> ipAddress;

    int port = 5555;
//    prompt << "Please specify server port comunication" << std::endl;
//    prompt;
//    std::cin >> port;

    return Host(ipAddress, port);
}

void manual(const ConfigManagerPtr &cm)
{
    std::cout << "==============================" << std::endl;
    prompt << "Server is starting ..." << std::endl;
    prompt << "Please select ip address for between server comunication:" << std::endl;

    QList<QHostAddress> allAdresses = QNetworkInterface::allAddresses();
    unsigned nb = 1, nb_c = 0;
    int port = 0;

            foreach(QHostAddress hostAddress, allAdresses) {
            if (hostAddress.protocol() != QAbstractSocket::IPv4Protocol ||
                hostAddress == QHostAddress(QHostAddress::LocalHost)) {
                allAdresses.removeOne(hostAddress);
            }
        }

    for (int i = 0; i < allAdresses.size(); ++i) {
            prompt << nb++ << ") " << allAdresses[i].toString().toStdString() << std::endl;
    }

    while (nb_c < 1 || nb_c >= nb) {
        prompt;
        std::cin >> nb_c;
    }

//    prompt << "Please specify server port comunication" << std::endl;
//    prompt;
//    std::cin >> port;

    cm->setListenHost(Host(allAdresses[nb_c-1].toString().toStdString(), SERVER_LISTEN_PORT_FOR_SERVER), ServerListenModule::getSectionName());
    cm->setListenHost(Host(allAdresses[nb_c-1].toString().toStdString(), SERVER_LISTEN_PORT_FOR_CLIENT), ClientListenModule::getSectionName());
    cm->setListenHost(Host(allAdresses[nb_c-1].toString().toStdString(), SERVER_LISTEN_PORT_FOR_NODE), NodeListenModule::getSectionName());

    char leader;
    prompt << "Is it first server in this net [y/n]" << std::endl;
    prompt;
    std::cin >> leader;
    if (toupper( leader ) == 'Y') {
        cm->removeAllHostsFromSection(ServerListenModule::getSectionName());
    } else {
        cm->removeAllHostsFromSection(ServerListenModule::getSectionName());
        cm->addHostToSection(addListener(), ServerListenModule::getSectionName());
    }
    cm->addHostToSection(Host(allAdresses[nb_c-1].toString().toStdString(), SERVER_LISTEN_PORT_FOR_SERVER), ServerListenModule::getSectionName()); // add own address to hosts
}

MessageRound roundIn, roundOut;
Responder responder;
ServerDBConnector serverDBConnector;

void onSuccessfulAuthentication() {
    responder.start();
    serverDBConnector.start();
}

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    a.setQuitOnLastWindowClosed(false);

    QCoreApplication::setApplicationName("rso_server");
    QCoreApplication::setApplicationVersion("0.0.1");

    QCommandLineParser parser;
    parser.setApplicationDescription("This is server");
    parser.addHelpOption();
    parser.addVersionOption();
    QCommandLineOption manualConfigurationOption(QStringList() << "m" << "manual", "Manual configuration for server");
    parser.addOption(manualConfigurationOption);
    
    CredentialsReader credentialsReader;
    credentialsReader.initialize(parser);
    parser.process(a);
    Credentials credentials = credentialsReader.read(parser, a);

    // Manager konfiguracji wspolny dla modulow serwera (wszystkie moduly maja 1 wspolny plik konfiguracyjny)
    ConfigManagerPtr cm = std::make_shared<ConfigurationManager>(SERVER_CONFIG_FILE_NAME);

    parser.process(a);
    if (parser.isSet(manualConfigurationOption)) {
        manual(cm);
    }

    responder.setMessageRoundOut(&roundOut);
    responder.setMessageRoundIn(&roundIn);
    serverDBConnector.setMessageRoundIn(&roundIn);
    serverDBConnector.setMessageRoundOut(&roundOut);

    // Modul nasluchujacy polaczen od wezlow
    NodeListenModule nodeModule(cm, &roundOut);

    // Modul nasluchujacy polaczen od innych serwerow
    ServerListenModule serverModule(cm, credentials);
    serverModule.listenToConnectionsFrom(serverModule.SERVER_SECTION);

    // Modul nasluchujacy polaczen od klientow
    ClientListenModule clientModule(cm, &roundIn);
    responder.setCommunicationModule(&clientModule);

    QObject::connect(&nodeModule, SIGNAL(receivedFaultyServerList(const std::list<Host>&)),
                     &serverModule, SLOT(checkServers(const std::list<Host>&)));

    QObject::connect(&serverModule, SIGNAL(fun()), &clientModule, SLOT(listenToConnectionsFromClients()));
    QObject::connect(&serverModule, SIGNAL(fun()), &nodeModule, SLOT(listenToConnectionsFromNodes()));

    serverModule.start(&onSuccessfulAuthentication);
    return a.exec();
}
