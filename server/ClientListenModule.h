#ifndef RSO_CLIENTLISTENMODULE_H
#define RSO_CLIENTLISTENMODULE_H

#include <QObject>
#include <cmloggerwindow.h>
#include "CommunicationModule.h"

class CMLoggerWindow;
class MessageRound;

class ClientListenModule : public CommunicationModule {
    Q_OBJECT

public:
//    ClientListenModule(ConfigManagerPtr manager, MessageRound *msgQueue, QObject *parent = 0)
//        : CommunicationModule(manager, parent), messageQueue(msgQueue) {}

    ClientListenModule(ConfigManagerPtr manager, MessageRound *msgQueue, QObject *parent = 0);
    ~ClientListenModule();

    const std::string SERVER_SECTION = "server";
    const std::string CLIENT_SECTION = "client";
    static std::string getSectionName() { return "client"; }
    bool sendToClientAndWait(Message &message);
    void addServersList(Message& message);

    CMLoggerWindow *cmLogger;

protected:
    virtual void onReceive(Message& message);
    virtual void onMessageSent(const Host& receiver, const Message& message);
    virtual void onFailedToSendMessage(const Host& receiver, const Message& message);

private:
    std::list<Host> getServers();
    std::list<Host> getClients();
    void addClient(const Host& host);
    MessageRound *messageQueue;

    Host getMyInterface();
    void handledCaseLog(const int &type);

public slots:
    void listenToConnectionsFromClients();
    //void sendToClient(const Message &message);
};

#endif //RSO_CLIENTLISTENMODULE_H
