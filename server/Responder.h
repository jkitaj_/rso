#ifndef RSO_RESPONDER_H
#define RSO_RESPONDER_H

#include <thread>
#include <chrono>
#include <iostream>
#include "database.h"
#include "MessageRound.h"
#include <Message.h>
#include "Runnable.h"
#include "ClientListenModule.h"




class Responder: public Runnable{
	Message message;
	MessageRound* roundOut, *roundIn;
	ClientListenModule *module;

public:
    Responder()
            : Runnable(),
              message(), module(nullptr) {}
    ~Responder() {};
    void setMessageRoundOut(MessageRound* rd){roundOut=rd;}
    void setMessageRoundIn(MessageRound* rd){roundIn=rd;}
    void setCommunicationModule(ClientListenModule *m){ module=m; }

protected:
    void run();
};

void Responder::run(){
	while(run_){
		if (module!=nullptr){
			Message message(-1);
			roundOut->getMessage(message);
			if(message.getResponse()!="Wrong Pass") {
				module->addServersList(message);
			}
			int i=0;
			while(module->sendToClientAndWait(message)!=true && i<5){
				++i;
				std::cout<<"Problem with sending message to Client, retrying in 3 sec...\n";
				std::this_thread::sleep_for(std::chrono::milliseconds(3000));
			}
			if(i<5){
				std::cout<<"Message sent to the Client.\n";
			}else{
				message.setState(Message::State::ERROR);
				message.setInfo("Client unreachable.");
			}
			roundIn->setMessage(message);
		}
	}
}
#endif
