#ifndef RSO_SERVERLISTENMODULE_H
#define RSO_SERVERLISTENMODULE_H

#include <QObject>
#include "CommunicationModule.h"
#include "Credentials.h"
#include <thread>

class SSLoggerWindow;

class ServerListenModule : public CommunicationModule {
    Q_OBJECT
public:
    typedef void (*OnAuthenticateFunction)();
    enum State {
        UNINITIALIZED=0,
        INITIALIZING,
        DEINITIALIZING,
        STOPPED,
        STARTING,
        RUNNING,
        STOPPING,
        PAUSING,
        PAUSED,
        RESUMING,
        ADDED,
        ADDING
    };

    enum Role {
        LEADER,
        SLAVE,
        NEW,
        NEW_AUTHENTICATED,
        NEW_NOAUTHENTICATED,
        CLEADER, // CANDIDATE FOR LEADER
    };

    ServerListenModule(ConfigManagerPtr manager, const Credentials& credentials_, QObject *parent = 0);
    void start(OnAuthenticateFunction func);
    ~ServerListenModule();
    const std::string SERVER_SECTION = "server";
    static std::string getSectionName() { return "server"; }

    SSLoggerWindow *cmLogger;

protected:
    virtual void onReceive(Message& message);
    virtual void onMessageSent(const Host& receiver, const Message& message);
    virtual void onFailedToSendMessage(const Host& receiver, const Message& message);

private:
    OnAuthenticateFunction onAuthenticationFunction;
    State state;
    Role role;
    Credentials credentials;

    std::list<Host> getServers();
    std::list<Host> getServersOverMe();
    void addServer(const Host &server);
    void removeServer(const Host &server);
    void addServers(const std::list<Host> &servers);
    void removeAllServers();
    struct SortDescendingPriority {
        bool operator()(const Host &first, const Host &second)
        {
            return first.getLastPartAddress() > second.getLastPartAddress();
        }
    };

    bool removeSmallerPriority(const Host &main, const Host &second) {
        return main.getLastPartAddress() > second.getLastPartAddress();
    }

    //// setters
    void setState(const State state) { this->state = state; }
    void setRole(const Role role) { this->role = role; }

    //// getters
    // role
    bool isLeader() const { return role == Role::LEADER; }
    bool isSlave() const { return role == Role::SLAVE; }
    bool isNew() const { return role == Role::NEW; }
    bool isNewAuthenticated() const { return role == Role::NEW_AUTHENTICATED; }
    bool isCandidatLeader() const { return role == Role::CLEADER; }

    Host& getMyLeader() { return getServers().front(); }
    Host getMyInterface();

    /* for test */
    std::thread state_thread;
    std::thread adding_thread;

    void tryJoinToServers();

    // logger
    void stateLogLoop();
    void stateLog();
    void sendLog(const Host host, const Message &message);
    void unhandledCase(const Message &message);
    void handledMessageType(const int &type);
    void handledCaseLog(const int &type);

    void doHostsSychronization();
    void doHostsElection();

    bool authenticate(const std::string& login, const std::string& password);
    void onSuccessfulAuthentication();

    void checkIfServersAreAlive(const std::list<Host> &servers);
    void forwardServerListToLeader(const std::list<Host> &servers);

signals:
    void fun();

public slots:
    void checkServers(const std::list<Host>& servers);
};

#endif //RSO_SERVERLISTENMODULE_H
