#include "ServerListenModule.h"
#include <iostream>
#include <future>
#include <sstream>
#include <database.h>

#include "ssloggerwindow.h"

#define consoleLog 0

#if consoleLog
    #define promptBeg std::cout<<"server-ss> "
    #define prompt std::cout
#else
std::stringstream prompt;
std::stringstream &promptBeg = prompt;
#endif

ServerListenModule::~ServerListenModule()
{
    delete cmLogger;
}

ServerListenModule::ServerListenModule(ConfigManagerPtr manager,
                                       const Credentials& credentials_,
                                       QObject *parent)
        : CommunicationModule(manager, parent),
          state(State::UNINITIALIZED),
          role(Role::NEW),
          credentials(credentials_)
//          state_thread(&ServerListenModule::stateLogLoop, this)
{ }

void ServerListenModule::start(OnAuthenticateFunction func) {
    this->onAuthenticationFunction = func;

    cmLogger = new SSLoggerWindow();
    cmLogger->show();
    cmLogger->setRole(role);
    cmLogger->setWindowTitle(cmLogger->windowTitle() + " "
                             + QString::fromStdString(getMyInterface().getAddress())
                             + ":"
                             + QString::number(getMyInterface().getPort())
    );

    if (getServers().front() == getMyInterface()) { // THis is first server / default leader
        if(authenticate(credentials.getLogin(), credentials.getPassword())) {   // authenticate myself :P
            onSuccessfulAuthentication();
            setRole(Role::LEADER);
            cmLogger->setRole(role);
            promptBeg << "I am leader" << std::endl;
            setState(State::RUNNING);

            Message message;
            std::list<Host> servers = getServers(); // all known servers
            ////// to DB
            DataBase base;
            base.updateServerList(servers);
        } else {
            setRole(Role::NEW_NOAUTHENTICATED);
            setState(State::STOPPING);
            exit(0);
        }
    } else {
        setRole(Role::NEW);
        cmLogger->setRole(role);
        promptBeg << "I am new" << std::endl;
        setState(State::ADDING);

        adding_thread = std::thread(&ServerListenModule::tryJoinToServers, this);
    }

    cmLogger->setRole(role);
    stateLog();
}

void ServerListenModule::onReceive(Message& message_) {
    cmLogger->appednMessage(CMLoggerWindow::INCOMING, message_, getMyInterface(), role);

#if consoleLog==0
    prompt.str(std::string());
#endif

    Message message = message_;
    qDebug() << "ServerListenModule: I received message" << message;
    promptBeg << std::string(5, '=') << " onReceive " << std::string(5, '=') << std::endl;
    Host sender = message.getSender();
    int mType = message.getType();

    switch (mType) {
        case Message::Type::SERVER_REQADD:
            if (isLeader()) { // TODO: test it
                if (authenticate(message.getLogin(), message.getPassword())) {
                    handledCaseLog(mType);
                    addServer(message.getServer());
//                    message.setType(Message::SERVER_RESPADDED);
//                    message.setSender(getMyInterface());
//                    send(message.getServer(), message);

//                    if (etLastPartAddress() > getMyInterface().getLastPartAddress()) { // new leader?
//
//                    } else {
                    // REQ HOST SYNC
                    doHostsSychronization();
//                    }
                } else {
                    unhandledCase(message);
                }
            } else if (isSlave()) { // TODO: check it // TODO: test it
                handledCaseLog(mType);
                qDebug() << "I got request for add but i am not a leader";
                qDebug() << message;

//                Q_ASSERT(sender != getMyInterface());

                // resend to my leader
                message.setType(Message::Type::SERVER_REQADD);
                message.setServer(sender);
                message.setSender(getMyInterface());
                send(getMyLeader(), message);
            } else if (isNew()){
                handledCaseLog(mType);
                promptBeg << "wtf i am new ?" << std::endl;
                message.setType(Message::Type::SERVER_RESPNOTLEADER);
                message.setSender(getMyInterface());
                send(sender, message);
            } else if (isCandidatLeader()) {
                handledCaseLog(mType);
                promptBeg << "Ignoring ..." << std::endl;
            } else {
                unhandledCase(message);
            }
            break;
        case Message::Type::SERVER_RESPNAUTH:
            if (isLeader()) {
                handledCaseLog(mType);
                qDebug() << "Fatal error. Leader got " << message;
            } else if (isSlave()) { // TODO: test it
                handledCaseLog(mType);
                setRole(Role::NEW_NOAUTHENTICATED);
                setState(State::STOPPING);
            }
            break;
        case Message::SERVER_REQHSYNC:
            if (isLeader()) { // TODO: test it
                if (sender == getMyInterface()) {
                    handledCaseLog(mType);
                    promptBeg << "Ignoring ..." << std::endl;
                } else {
                    handledCaseLog(mType);
                    doHostsSychronization();
                }
            } else if (isSlave()) { // TODO: test it
                handledCaseLog(mType);
                removeAllServers();
                addServers(message_.getServers());
            } else if (isNew()) { // TODO: test it
                // leader accepted my login and password
                onSuccessfulAuthentication();

                handledCaseLog(mType);
                setRole(Role::SLAVE);
                setState(State::RUNNING);
                removeAllServers();
                addServers(message_.getServers());
            } else {
                unhandledCase(message);
            }
            break;
        case Message::Type::SERVER_REQELEC:
            if (isLeader()) { // TODO: test it
                handledCaseLog(mType);
                setRole(Role::CLEADER);
                message.setSender(getMyInterface());
                message.setType(Message::Type::SERVER_RESPELECSORRY);
                send(sender, message);

                doHostsElection();
            } else if (isSlave()) { // TODO: test it
                handledCaseLog(mType);
                setRole(Role::CLEADER);
                message.setSender(getMyInterface());
                message.setType(Message::Type::SERVER_RESPELECSORRY);
                send(sender, message);

                doHostsElection();
            } else if (isCandidatLeader()) { // TODO: test it
                handledCaseLog(mType);
                message.setSender(getMyInterface());
                message.setType(Message::Type::SERVER_RESPELECSORRY);
                send(sender, message);

                doHostsElection();
            } else if(isNew()) {
                handledCaseLog(mType);
                handledCaseLog(mType);
                promptBeg << "wtf @ i am new ?" << std::endl;
                message.setType(Message::Type::SERVER_RESPNOTLEADER);
                message.setSender(getMyInterface());
                send(sender, message);
            } else {
                unhandledCase(message);
            }
            break;
        case Message::Type::SERVER_RESPELECSORRY:
            if (isLeader()) { // TODO: check it // TODO: test it
                if(sender == getMyInterface()) {
                    handledCaseLog(mType);
                    promptBeg << "Ignoring ..." << std::endl;
                } else {
                    handledCaseLog(mType);
                    setRole(Role::SLAVE);
                }
            } else  if (isCandidatLeader()) { // TODO: test it
                handledCaseLog(mType);
                setRole(Role::SLAVE);
            } else {
                unhandledCase(message);
            }
            break;
        case Message::Type::SERVER_RESPNOTLEADER:
            if (isLeader()) { // TODO: check it // TODO: test it
                promptBeg << "Ignoring ..." << std::endl;
            } else if (isCandidatLeader()) { // TODO: test it
                handledCaseLog(mType);
                removeServer(sender);

                doHostsElection();
            } else  if (isSlave()) { // TODO: test it
                handledCaseLog(mType);
                removeServer(sender);

                doHostsElection();
            } else {
                unhandledCase(message);
            }
            break;
        case Message::Type::SERVER_LIST_TO_CHECK:
            if(isLeader()) {
                checkIfServersAreAlive(message.getServers());
            } else {    // shouldn't happen!
                forwardServerListToLeader(message.getServers());
            }
        default:
            unhandledCase(message);
            break;
    }

    stateLog();

#if consoleLog==0
    cmLogger->setMessageInfo(prompt.str(), role);
#endif
}

void ServerListenModule::onFailedToSendMessage(const Host &receiver, const Message &message_) {
    cmLogger->appednMessage(CMLoggerWindow::MISSED, message_, receiver, role);
#if consoleLog==0
    prompt.str(std::string());
#endif
    // failed to sent message to receiver -> receiver might be dead
    qDebug() << "ServerListenModule: Failed to send message" << message_;
    qDebug() << "ServerListenModule: receiver" << receiver;

    prompt << std::string(5, '=') << " onFailedToSendMessage " << std::string(5, '=') << std::endl;

    Message message = message_;
    Host sender(message.getSenderIP(), message.getSenderPort());

    int mType = message.getType();

    if (isSlave() && getMyLeader() == receiver) {
        handledCaseLog(mType);
        promptBeg << "Failed to send to my leader !!! I remove him." << std::endl;
        removeServer(receiver);
        setRole(Role::CLEADER);

        message.setSender(getMyInterface());
        message.setType(Message::Type::SERVER_REQELEC);
        send(getMyLeader(), message);

        doHostsElection();

#if consoleLog==0
        cmLogger->setMessageInfo(prompt.str(), role);
#endif

        return;
    }

    switch (mType) {
        case Message::Type::SERVER_REQELEC:
            if (isCandidatLeader()){ // TODO: check it // TODO: test it
                //         TODO: prepare election
                removeServer(receiver);

                std::list<Host> servers = getServersOverMe();
                if (servers.empty()) { // yeah, i am leader
                    handledCaseLog(mType);
                    setRole(Role::LEADER);

                    std::list<Host> servers = getServers();

                    servers.sort([](const Host &first, const Host &second) {
                        return first.getLastPartAddress() > second.getLastPartAddress();
                    });

                    removeAllServers();
                    addServers(servers);

                    doHostsSychronization();
                } else {
                    unhandledCase(message);
                }
            } else {
                unhandledCase(message);
            }
            break;
        case Message::Type::SERVER_REQADD:
            if (isNew() && state == State::ADDING) { // TODO: test it
                handledCaseLog(mType);
                static unsigned try_ = 0;
                promptBeg << "Left trys: " << 5 - try_ % 5 << std::endl;
                if (!(++try_ % 5)) {
                    setState(State::STOPPED);
                }
            } else if (isLeader()) {
                handledCaseLog(mType);
                promptBeg << "Ignoring ..." << std::endl;
            } else {
                unhandledCase(message);
            }
            break;
        case Message::Type::SERVER_CHECK_IF_ALIVE:
            if(isLeader()) {
                promptBeg << "Checked that " << receiver << " is dead - remove him from server list";
                std::list<Host> servers = getServers();

                Host rtemp = receiver;
                rtemp.setPort(5555);    // change port from 6666 to 5555 to correctly remove
                servers.remove(rtemp);

                removeAllServers();
                servers.sort([](const Host &first, const Host &second) {
                    return first.getLastPartAddress() > second.getLastPartAddress();
                });
                addServers(servers);
                doHostsSychronization();
            } else {
                unhandledCase(message);
            }
            break;
        default:
            unhandledCase(message);
            break;
    }
    stateLog();

#if consoleLog==0
    cmLogger->setMessageInfo(prompt.str(), role);
#endif
}

void ServerListenModule::onMessageSent(const Host &receiver, const Message &message_) {
    cmLogger->appednMessage(CMLoggerWindow::OUTGOING, message_, receiver, role);
#if consoleLog==0
    prompt.str(std::string());
#endif

    promptBeg << std::string(5, '=') << " onMessageSent " << std::string(5, '=') << std::endl;
    sendLog(receiver, message_);

    stateLog();

#if consoleLog==0
    cmLogger->setMessageInfo(prompt.str(), role);
#endif
}

void ServerListenModule::addServer(const Host &server) {
    promptBeg << " + add to server list " << server << std::endl;
    getConfigurationManager()->addHostToSection(server, SERVER_SECTION);
}
void ServerListenModule::removeServer(const Host &server) {
    promptBeg << " - remove from server list " << server << std::endl;
    getConfigurationManager()->removeHostFromSection(server, SERVER_SECTION);
}
void ServerListenModule::addServers(const std::list<Host> &servers) {
    promptBeg << " + add list of servers (" << servers.size() << ")" << std::endl;

    for (auto server : servers) {
        promptBeg << server << std::endl;
    }

    getConfigurationManager()->addHostsToSection(servers, SERVER_SECTION);
}
void ServerListenModule::removeAllServers()
{
    promptBeg << " - remove all servers (" << getServers().size() << ")" << std::endl;

    std::list<Host> servers=getServers();
    for (std::list<Host>::iterator it=servers.begin(); it!=servers.end(); ++it) {
        promptBeg << *it << std::endl;
    }

    getConfigurationManager()->removeAllHostsFromSection(SERVER_SECTION);
}

void ServerListenModule::tryJoinToServers()
{
    while (this->state == State::ADDING) {
        Message message(Message::Type::SERVER_REQADD);
        message.setServer(getMyInterface());
        message.setSender(getMyInterface());
        message.setLogin(credentials.getLogin());
        message.setPassword(credentials.getPassword());
        send(getMyLeader(), message);

        std::this_thread::sleep_for(std::chrono::seconds(3));
    }
}

// getters
std::list<Host> ServerListenModule::getServers() {
    return getConfigurationManager()->getHostsFromSection(SERVER_SECTION);
}

std::list<Host> ServerListenModule::getServersOverMe() {
    std::list<Host> hosts = getServers();

    unsigned myPriority = getMyInterface().getLastPartAddress();
    hosts.remove_if([&myPriority](const Host &second) {
        return myPriority >= second.getLastPartAddress();
    });

    hosts.sort([](const Host &first, const Host &second) {
        return first.getLastPartAddress() > second.getLastPartAddress();
    });

    return hosts;
}

Host ServerListenModule::getMyInterface() {
    return getConfigurationManager()->getCurrentConfiguration().getSectionByName(SERVER_SECTION).listen;
}

// node sent the message with the list of servers that it could not connect to ->
// check if servers are not dead
void ServerListenModule::checkServers(const std::list<Host> &servers) {
    if(isLeader()) {
        checkIfServersAreAlive(servers);
    } else {
        forwardServerListToLeader(servers);
    }
}

void ServerListenModule::checkIfServersAreAlive(const std::list<Host> &servers) {
    Q_ASSERT(isLeader());
    promptBeg << "Leader: Received message with list of servers to check";
    Message msg(Message::SERVER_CHECK_IF_ALIVE);
    msg.setSender(getMyInterface());
    for(const Host& h : servers) {
        promptBeg << "Checking..." << h;
        send(h, msg);
    }
}

void ServerListenModule::forwardServerListToLeader(const std::list<Host> &servers) {
    promptBeg << "Slave: Received message with list of servers to check, forward to leader...";
    Message msg(Message::SERVER_LIST_TO_CHECK);
    msg.setSender(getMyInterface());
    msg.addServers(servers);
    send(getMyLeader(), msg);
}

void ServerListenModule::unhandledCase(const Message &message)
{
    promptBeg << std::endl;
    promptBeg << std::string(20, '=') << " Unhandled case " << std::string(20, '=') << std::endl;
    stateLog();
    handledMessageType(message.getType());
    qDebug() << message;
    promptBeg << std::string(20, '=') << " End unhandled case " << std::string(20, '=') << std::endl;
    promptBeg << std::endl;
}

void ServerListenModule::doHostsSychronization()
{
    if (isLeader()) {
        Message message;
        std::list<Host> servers = getServers(); // all known servers

        message.setType(Message::SERVER_REQHSYNC);
        message.setSender(getMyInterface());
        message.appendServers(servers);

        std::list<Host> serversForDB = message.getServers();

        if (!servers.empty()) {
            for (std::list<Host>::iterator it=servers.begin(); it != servers.end(); ++it) {
                send(*it, message);
            }
        }

        ////// to DB
        DataBase base;
        base.updateServerList(serversForDB);

    } else {
        promptBeg << "Only Leaer can make a hosts synchronization" << std::endl;
    }
}

void ServerListenModule::doHostsElection()
{
    promptBeg << "Start Host election" << std::endl;

    if (getServers().size() == 1) {
        setRole(Role::LEADER);
        return;
    }

    setRole(Role::CLEADER);
    Message message;

    std::list<Host> servers = getServersOverMe();
    if (servers.empty()) {
        promptBeg << "No one over me. I am leader." << std::endl;
        setRole(Role::LEADER);

        std::list<Host> servers = getServers();

        servers.sort([](const Host &first, const Host &second) {
            return first.getLastPartAddress() > second.getLastPartAddress();
        });

        removeAllServers();
        addServers(servers);

        doHostsSychronization();
    } else {

        servers.sort([](const Host &first, const Host &second) {
            return first.getLastPartAddress() > second.getLastPartAddress();
        });

        message.setSender(getMyInterface());
        message.setType(Message::Type::SERVER_REQELEC);

        for (std::list<Host>::iterator it=servers.begin(); it != servers.end(); ++it) {
            send((*it), message);
        }
    }
}

// logger
void ServerListenModule::stateLogLoop()
{
    while (true) {
        stateLog();
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }
}

void ServerListenModule::sendLog(const Host host, const Message &message)
{
    promptBeg << "Sending to " << host << std::endl;
    handledMessageType(message.getType());
}

void ServerListenModule::stateLog() {
    promptBeg << "State: ";
    switch (state) {
        case State::UNINITIALIZED:
            prompt << "UNINITIALIZED";
            break;
        case State::INITIALIZING:
            prompt << "INITIALIZING";
            break;
        case State::DEINITIALIZING:
            prompt << "DEINITIALIZING";
            break;
        case State::STOPPED:
            prompt << "STOPPED";
            break;
        case State::STARTING:
            prompt << "STARTING";
            break;
        case State::RUNNING:
            prompt << "RUNNING";
            break;
        case State::STOPPING:
            prompt << "STOPPING";
            break;
        case State::PAUSING:
            prompt << "PAUSING";
            break;
        case State::PAUSED:
            prompt << "PAUSED";
            break;
        case State::RESUMING:
            prompt << "RESUMING";
            break;
        case State::ADDED:
            prompt << "ADDED";
            break;
        case State::ADDING:
            prompt << "ADDING";
            break;
        default:
            prompt << "?|?|?|?|?|?";
            break;
    }

    prompt << " Role: ";

    switch (role) {
        case Role::LEADER:
            prompt << "LEADER";
            break;
        case Role::SLAVE:
            prompt << "SLAVE";
            break;
        case Role::NEW:
            prompt << "NEW";
            break;
        case Role::NEW_AUTHENTICATED:
            prompt << "NEW_AUTHENTICATED";
            break;
        case Role::NEW_NOAUTHENTICATED:
            prompt << "NEW_NOAUTHENTICATED";
            break;
        case Role::CLEADER:
            prompt << "CLEADER";
            break;
        default:
            prompt << "?|?|?|?|?|?";
            break;
    }

    prompt << std::endl;
}

void ServerListenModule::handledMessageType(const int &type) // TODO: delete it
{
    promptBeg << "Message: ";
    switch (type) {
        case Message::Type::UNDEFINED:
            prompt << "UNDEFINED";
            break;
        case Message::Type::CLIENT_REQUEST:
            prompt << "CLIENT_REQUEST";
            break;
        case Message::Type::SERVER_PEERSYNC:
            prompt << "SERVER_PEERSYNC";
            break;
        case Message::Type::SERVER_REQADD:
            prompt << "SERVER_REQADD";
            break;
        case Message::Type::SERVER_REQADDING:
            prompt << "SERVER_REQADDING";
            break;
        case Message::Type::SERVER_REQHSYNC:
            prompt << "SERVER_REQHSYNC";
            break;
        case Message::Type::SERVER_RESPADDED:
            prompt << "SERVER_RESPADDED";
            break;
        case Message::Type::SERVER_RESADDING:
            prompt << "SERVER_RESADDING";
            break;
        case Message::Type::SERVER_RESPAUTH:
            prompt << "SERVER_RESPAUTH";
            break;
        case Message::Type::SERVER_RESPNAUTH:
            prompt << "SERVER_RESPNAUTH";
            break;
        case Message::Type::SERVER_REQELEC:
            prompt << "SERVER_REQELEC";
            break;
        case Message::Type::SERVER_RESPELECSORRY:
            prompt << "SERVER_RESPELECSORRY";
            break;
        case Message::Type::SERVER_RESPNOTLEADER:
            prompt << "SERVER_RESPNOTLEADER";
            break;
        default:
            prompt << "?|?|?|?|?|?";
            break;
    }

    prompt << std::endl;
}

void ServerListenModule::handledCaseLog(const int &type)
{
    stateLog(); handledMessageType(type);
}

bool ServerListenModule::authenticate(const std::string &login, const std::string &password) { // TODO: don't print password
    DataBase db;
    bool ok = db.checkHost(login, password);
    if(ok) prompt << "ServerListenModule: Authentication successful: login: " << login << " password: " << password << std::endl;
    else prompt << "ServerListenModule: Authentication failed: login: " << login << " password: " << password << std::endl;
    return ok;
}

void ServerListenModule::onSuccessfulAuthentication() {
    std::cout << "ServerListenModule: Successfully authenticated!\n";
    onAuthenticationFunction();
    emit fun();
}
