#ifndef RSO_NODELISTENMODULE_H
#define RSO_NODELISTENMODULE_H

#include <QObject>
#include <cmloggerwindow.h>
#include "CommunicationModule.h"

class CMLoggerWindow;
class MessageRound;

class NodeListenModule : public CommunicationModule {
    Q_OBJECT

public:
//    NodeListenModule(ConfigManagerPtr manager, MessageRound *msgQueue, QObject *parent = 0)
//        : CommunicationModule(manager, parent), messageQueue(msgQueue) {}

    NodeListenModule(ConfigManagerPtr manager, MessageRound *msgQueue, QObject *parent = 0);

    ~NodeListenModule();

    const std::string NODE_SECTION = "node";

    static std::string getSectionName() { return "node"; }

    CMLoggerWindow *cmLogger;

protected:
    virtual void onReceive(Message& message);

private:
    MessageRound *messageQueue;

    Host getMyInterface();
    void handledCaseLog(const int &type);
    void unhandledCase(const Message &message);

    bool authenticate(const std::string& login, const std::string& password);

public slots:
    void listenToConnectionsFromNodes();

signals:
    void receivedFaultyServerList(const std::list<Host>& servers);
};

#endif //RSO_NODELISTENMODULE_H
