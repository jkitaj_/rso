#ifndef RSO_SERVERDBCONNECTOR_H
#define RSO_SERVERDBCONNECTOR_H

#include <thread>
#include <chrono>
#include <iostream>
#include "database.h"
#include "MessageRound.h"
#include <Message.h>
#include "Runnable.h"
class ServerDBConnector: public Runnable{
	int i;
	Message message;
	MessageRound* roundIn;
	MessageRound* roundOut;

public:
    ServerDBConnector()
            : Runnable(),
              message() {}
    ~ServerDBConnector() {};
    void setMessageRoundIn(MessageRound* rd){roundIn=rd;}
    void setMessageRoundOut(MessageRound* rd){roundOut=rd;}
protected:
    void run();
    
};


void ServerDBConnector::ServerDBConnector::run(){
	while(run_){
		Message message;
		roundIn->getMessage(message);

		/* Tymczasem puki Adrian nie poprawi*/
			//message.setLogin("admin");
			//message.setPassword("admin");


			DataBase base;
			if (message.getState() == Message::State::UNINITIALIZED) {
				if (base.checkHost(message)) {
					message.setState(Message::State::SENT);
					message.setResponse("SENT");
					while (base.insert_task(message) == false) {
						std::cout << "Can't add task to database, trying again in 3 second." << std::endl;
						std::this_thread::sleep_for(std::chrono::milliseconds(3000));
					}
				} else {
					std::cout << "Can't authorize message author." << std::endl;
					message.setResponse("Wrong Pass");
					message.setState(Message::State::ERROR);
				}
				roundOut->setMessage(message);

				/*******************************************/
				/*Addning new Task to DB*/
			} else if (message.getState() == Message::State::SENT) {
				message.setState(Message::State::INQUEUE);
				message.setResponse("INQUEUE");
				while (base.update_task(message) == false) {
					std::cout << "Can't add task to database, trying again in 3 second." << std::endl;
					std::this_thread::sleep_for(std::chrono::milliseconds(3000));
				}

				roundOut->setMessage(message);
				/*******************************************/
				/*Updating status of sent message in database*/
			} else if (message.getState() == Message::State::INQUEUE) {
				while (base.update_task(message) == false) {
					std::cout << "Can't add task to database, trying again in 3 second." << std::endl;
					std::this_thread::sleep_for(std::chrono::milliseconds(3000));
				}
				/*******************************************/
				/*Updating status of sent message in database*/
			} else if (message.getState() == Message::State::READY) {
				message.setState(Message::State::SENDBACK);
				while (base.update_task(message) == false) {
					std::cout << "Can't add task to database, trying again in 3 second." << std::endl;
					std::this_thread::sleep_for(std::chrono::milliseconds(3000));
				}

			} else if (message.getState() == Message::State::ERROR) {
				while (base.update_task(message) == false) {
					std::cout << "Can't add task to database, trying again in 3 second." << std::endl;
					std::this_thread::sleep_for(std::chrono::milliseconds(3000));
				}
			} else {
				std::cout << "Message status unrecognized, skiping task." << std::endl;
				message.print();
			}


	}
}
#endif
