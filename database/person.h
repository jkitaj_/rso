#ifndef _PERSON_H_
#define _PERSON_H_

#include "cassandra.h"
#include <stdlib.h>
#include <iostream>

class Person{
public:
  cass_int32_t id;
  const char* fname;
  const char* lname;
  const char* sex;
  const char* province;
  cass_double_t ernings[5];
  Person();
  //Person(Person &person);
  Person(cass_int32_t id_, const char* fname_, const char* lname_, const char* sex_, const char* province_);
  void print();
};

#endif
