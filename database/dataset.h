#ifndef _DATASET_H_
#define _DATASET_H_

#include <string>
#include <vector>

class Person;

class DataSet{
	private:
		std::vector<Person*> data_;
		void sort(int idx, int left, int right);

	public:
		DataSet(){};
		~DataSet();
		std::string getAnalyse(int idx);
		void push_back(Person * p);
		Person* at(int i);
		int size(){data_.size();}
		double getSum(int idx);
		double getAvg(int idx);
		double getMin(int idx);
		double getMax(int idx);
		double getMedian(int idx);
		double count(int idx);
};

#endif
