#ifndef _DATABASE_CPP_
#define _DATABASE_CPP_

#include "cassandra.h"
#include "dataset.h"
#include <vector>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include "Message.h"

class Person;

class DataBase{
	private:
		CassCluster* cluster;
		CassSession* session;
		CassFuture* close_future;
		CassFuture* future;
		void print_error(CassFuture* future);
		CassError connect_session( );
		CassCluster* create_cluster();
		CassError execute_query( const char* query);
		bool updateDatabaseTimestamp();
	public:
		DataBase();
		~DataBase();
		CassError insert_into_person( const Person* person);
		CassError select_from_person(DataSet &person);
		CassError select_from_person(const char* query, DataSet &person);
		bool update_task(const Message& message);
		bool insert_task( const Message& message);
		bool select_task_to_work(Message& message, int state);
		bool checkHost(Message& message);
		bool checkHost(const std::string& login, const std::string& password);
		long getDatabaseTime();
		bool getServerList(std::list<Host> &servers);
		bool updateServerList(std::list<Host> servers);
		bool clearServerList();
};

#endif

