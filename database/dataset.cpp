#include "dataset.h"
#include "cassandra.h"
#include <stdlib.h>
#include <iostream>
#include "person.h"

DataSet::~DataSet(){
	int i=0;
	while(i<data_.size()){
		delete data_[i];
		i++;	
	}
}
void DataSet::push_back(Person * p){
	data_.push_back(p);
}

Person* DataSet::at(int i){
	return data_.at(i);
}

double DataSet::getSum(int idx){
	int i=0;
	double sum=0.0;
	while(i<this->data_.size()){
		sum+=data_[i]->ernings[idx];
		++i;
	}
	return sum;
}

double DataSet::getAvg(int idx){
	
	return getSum(idx)/data_.size();
}

double DataSet::getMin(int idx){
	int i=0;
	double min=data_[i]->ernings[idx];
	while(i<data_.size()){
		if(data_[i]->ernings[idx]<min) min=data_[i]->ernings[idx];
		++i;	
	}
	return min;
}

double DataSet::getMax(int idx){
	int i=0;
	double max=data_[i]->ernings[idx];
	while(i<data_.size()){
		if(data_[i]->ernings[idx]>max) max=data_[i]->ernings[idx];
		++i;	
	}
	return max;
}
double DataSet::count(int idx) {return data_.size();};

void DataSet::sort(int idx, int left, int right )
{
	
    int i = left;
    int j = right;
    double x = this->data_[( left + right ) / 2 ]->ernings[idx];
	//std::cout<<i<<"  "<<j<<"   "<<x<<std::endl;
    do
    {
        while( this->data_[i]->ernings[idx] < x )
             i++;
        
        while( this->data_[j]->ernings[idx] > x )
             j--;
        
        if( i <= j )
        {
	 	// std::cout<<"To tu0"<<std::endl;
            std::swap( this->data_[i],  this->data_[ j ] );
         //   std::cout<<"To tu"<<std::endl;
            i++;
            j--;
        }
    } while( i <= j );
    
    if( left < j ) sort( idx, left, j );
    
    if( right > i ) sort( idx, i, right );
    
}

double DataSet::getMedian(int idx){
	sort(idx, 0, data_.size()-1);
	
	if(data_.size()==1)
	{	
		std::cout<<"size 1"<<std::endl;
		return data_[0]->ernings[idx];
	}else if(data_.size()==2)
	{
		return (data_[1]->ernings[idx]+data_[0]->ernings[idx])/2;
	}

	if(data_.size()%2==1){
		return data_[data_.size()/2]->ernings[idx];
	}else{
		return (data_[data_.size()/2]->ernings[idx]+data_[data_.size()/2-1]->ernings[idx])/2;
	}
		
;
}


std::string DataSet::getAnalyse(int idx){
	/*int size(){data_.size();}
		double getSum(int idx);
		double getAvg(int idx);
		double getMin(int idx);
		double getMax(int idx);
		double getMedian(int idx);
		double count(int idx);*/
	std::string response=std::to_string(this->getAvg(idx));
	
	return response;


}