#include "database.h"
#include <chrono>
#include <thread>
#include "person.h"
#include <iostream>

DataBase::DataBase(){
	this->cluster=  NULL;
	this->session=  NULL;
	this->close_future = NULL;
	while(this->cluster==NULL){
		this->cluster= create_cluster();
		if(this->cluster==NULL){
			std::cout<<"Problem with creating cluster for DataBase"<<std::endl;
			std::this_thread::sleep_for(std::chrono::milliseconds(3000));
		}
	}
	while(this->session==NULL){
		this->session= cass_session_new();
		if(this->session==NULL){
			std::cout<<"Problem with creating new session for DataBase"<<std::endl;
			std::this_thread::sleep_for(std::chrono::milliseconds(3000));
		}
	}

	if (connect_session() != CASS_OK) {
		cass_cluster_free(this->cluster);
		cass_session_free(this->session);
		std::cout<<"Problem with connection to DataBase exiting..."<<std::endl;
		exit(-1);
	}
}
DataBase::~DataBase(){
	this->close_future = cass_session_close(this->session);
	cass_future_wait(this->close_future);
	cass_future_free(this->close_future);
	cass_cluster_free(this->cluster);
	cass_session_free(this->session);
}

void DataBase::print_error(CassFuture* future) {
	const char* message;
	size_t message_length;
	cass_future_error_message(future, &message, &message_length);
	fprintf(stderr, "Error: %.*s\n", (int)message_length, message);
}

CassCluster* DataBase::create_cluster() {
	CassCluster* cluster = cass_cluster_new();
	std::string line;
	std::ifstream namefile("ip.txt");
	if (namefile.is_open())
	{
		getline (namefile,line);
	}else{ std::cout<<"Cant open file with DataBase IP, exiting ..."<<std::endl; exit(-1);}
	cass_cluster_set_contact_points(cluster, line.c_str());
	return cluster;
}

CassError DataBase::connect_session() {
	CassError rc = CASS_OK;
	this->future = cass_session_connect(this->session, this->cluster);

	cass_future_wait(this->future);
	rc = cass_future_error_code(this->future);
	if (rc != CASS_OK) {
		print_error(this->future);
	}
	cass_future_free(this->future);

	return rc;
}

CassError DataBase::execute_query( const char* query) {
	CassError rc = CASS_OK;
	this->future = NULL;
	CassStatement* statement = cass_statement_new(query, 0);

	future = cass_session_execute(this->session,statement);
	cass_future_wait(this->future);

	rc = cass_future_error_code(this->future);
	if (rc != CASS_OK) {
		print_error(this->future);
	}

	cass_future_free(this->future);
	cass_statement_free(statement);

	return rc;
}

bool DataBase::update_task(const Message& message){
	CassError rc = CASS_OK;
	CassStatement* statement = NULL;
	this->future = NULL;
	const char* query = "UPDATE mykeyspace.task SET state = ?, updated=dateof(now()), reply=?, s_ip=?, n_ip=?, info=?  WHERE function =? and c_id =? AND c_ip=? AND c_login=? AND c_p=?;";
	statement = cass_statement_new(query, 10);

	cass_statement_bind_int32(statement,  0, message.getState());
	cass_statement_bind_string(statement, 1, message.getResponse().c_str());
	cass_statement_bind_string(statement, 2, message.getServerIP().c_str());
	cass_statement_bind_string(statement, 3, message.getNodeIP().c_str());
	cass_statement_bind_string(statement, 4, message.getInfo().c_str());
	cass_statement_bind_string(statement, 5, message.getQuery().c_str());
	cass_statement_bind_int32(statement, 6, (int)message.getID());
	cass_statement_bind_string(statement, 7, message.getClientIP().c_str());
	cass_statement_bind_string(statement, 8, message.getLogin().c_str());
	cass_statement_bind_int32(statement, 9, message.getClientPort());

	this->future = cass_session_execute(this->session,statement);
	cass_future_wait(this->future);

	rc = cass_future_error_code(this->future);
	if (rc != CASS_OK) {
		print_error(this->future);
		return false;
	}
	return true;
}
bool DataBase::insert_task(const Message& message) {

	CassError rc = CASS_OK;
	CassStatement* statement = NULL;
	this->future = NULL;
	const char* query = "INSERT INTO mykeyspace.task (state, updated, function, reply, info, c_id, c_ip, c_p, c_login, c_pass, s_ip, n_ip) VALUES  (?,dateof(now()), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	statement = cass_statement_new(query, 11);

	cass_statement_bind_int32(statement, 0, message.getState());
	cass_statement_bind_string(statement, 1, message.getQuery().c_str());
	cass_statement_bind_string(statement, 2, message.getResponse().c_str());
	cass_statement_bind_string(statement, 3, message.getInfo().c_str());
	cass_statement_bind_int32(statement, 4, message.getID());
	cass_statement_bind_string(statement, 5, message.getClientIP().c_str());
	cass_statement_bind_int32(statement, 6, message.getClientPort());
	cass_statement_bind_string(statement, 7, message.getLogin().c_str());
	cass_statement_bind_string(statement, 8, message.getPassword().c_str());
	cass_statement_bind_string(statement, 9, message.getServerIP().c_str());
	cass_statement_bind_string(statement, 10, message.getNodeIP().c_str());

	this->future = cass_session_execute(this->session,statement);
	cass_future_wait(this->future);

	rc = cass_future_error_code(this->future);
	if (rc != CASS_OK) {
		print_error(this->future);
		return false;
	}
	return true;
}

bool DataBase::select_task_to_work(Message& message, int state) {
	bool retVal=false;
	const char* func=NULL, *c_ip=NULL, *c_login=NULL,  *c_pass=NULL, *info=NULL, *n_ip=NULL, *reply=NULL, *s_ip=NULL;
	int c_id=-1, c_p=-1, st=-1;
	size_t length;
	CassError rc = CASS_OK;
	const char* query;
	CassStatement* statement = NULL;
	this->future = NULL;
	if(state == Message::State::INQUEUE){
		query = "SELECT * FROM mykeyspace.task WHERE state=? LIMIT 1 ALLOW FILTERING ;";
		statement = cass_statement_new(query,1);
		cass_statement_bind_int32(statement, 0, state);
	}else if(state==Message::State::INPROGRES){
		long time=this->getDatabaseTime() - 10000L;
		query = "SELECT * FROM mykeyspace.task WHERE state=? AND updated<? LIMIT 1 ALLOW FILTERING ;";
		statement = cass_statement_new(query,2);
		cass_statement_bind_int32(statement, 0, state);
		cass_statement_bind_int64(statement, 1, time);
	}else{
		return false;
	}
	
	future = cass_session_execute(this->session, statement);
	cass_future_wait(this->future);

	rc = cass_future_error_code(this->future);
	if (rc != CASS_OK) {
		print_error(this->future);
	}else{
		const CassResult* result = cass_future_get_result(this->future);
		CassIterator* iterator = cass_iterator_from_result(result);
		
		int i=0;
		while(cass_iterator_next(iterator)) {

			const CassRow* row = cass_iterator_get_row(iterator);
			//std::cout<<"poczatkowe"<<length<<std::endl;
			cass_value_get_string(cass_row_get_column(row, 0), &func, &length);
			//std::cout<<length<<std::endl;
			cass_value_get_int32(cass_row_get_column(row, 1), &c_id);
			cass_value_get_string(cass_row_get_column(row, 2), &c_ip, &length);
			//std::cout<<length<<std::endl;
			cass_value_get_string(cass_row_get_column(row, 3), &c_login, &length);
			//std::cout<<length<<std::endl;
			cass_value_get_int32(cass_row_get_column(row, 4), &c_p);
			cass_value_get_string(cass_row_get_column(row, 5), &c_pass, &length);
			//std::cout<<length<<std::endl;
			cass_value_get_string(cass_row_get_column(row, 6), &info, &length);
			//std::cout<<length<<std::endl;
			cass_value_get_string(cass_row_get_column(row, 7), &n_ip, &length);
			//std::cout<<length<<std::endl;
			cass_value_get_string(cass_row_get_column(row, 8), &reply, &length);
			//std::cout<<length<<std::endl;
			cass_value_get_string(cass_row_get_column(row, 9), &s_ip, &length);
			//std::cout<<length<<std::endl;
			cass_value_get_int32(cass_row_get_column(row, 10), &st);
			i++;
		}
		if(func!=NULL){
			message.setQuery(func);
			message.setID(c_id);
			message.setClientIP(c_ip);
			message.setLogin(c_login);
			message.setClientPort(c_p);
			message.setPassword(c_pass);
			message.setInfo(info);
			message.setNodeIP(n_ip);
			message.setResponse(reply);
			message.setServerIP(s_ip);
			message.setState(st);
			
			retVal=true;
		}


		cass_result_free(result);
		cass_iterator_free(iterator);
	}
	cass_future_free(this->future);
	cass_statement_free(statement);

	return retVal;
}

CassError DataBase::insert_into_person( const Person* person) {
	CassError rc = CASS_OK;
	CassStatement* statement = NULL;
	this->future = NULL;
	const char* query = "INSERT INTO mykeyspace.taxpayer (id,  fname, lname, sex, province, ernings_2010, ernings_2011, ernings_2012, ernings_2013, ernings_2014) VALUES  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

	statement = cass_statement_new(query, 10);

	cass_statement_bind_int32(statement, 0, person->id);
	cass_statement_bind_string(statement, 1, person->fname);
	cass_statement_bind_string(statement, 2, person->lname);
	cass_statement_bind_string(statement, 3, person->sex);
	cass_statement_bind_string(statement, 4, person->province);
	cass_statement_bind_double(statement, 5, person->ernings[0]);
	cass_statement_bind_double(statement, 6, person->ernings[1]);
	cass_statement_bind_double(statement, 7, person->ernings[2]);
	cass_statement_bind_double(statement, 8, person->ernings[3]);
	cass_statement_bind_double(statement, 9, person->ernings[4]);

	this->future = cass_session_execute(this->session,statement);
	cass_future_wait(this->future);

	rc = cass_future_error_code(this->future);
	if (rc != CASS_OK) {
		print_error(this->future);
	}

	cass_future_free(this->future);
	cass_statement_free(statement);

	return rc;
}

CassError DataBase::select_from_person(DataSet &person) {
	CassError rc = CASS_OK;
	CassStatement* statement = NULL;
	const char* key ="opolskie";
	this->future = NULL;
	const char* query = "SELECT * FROM mykeyspace.taxpayer WHERE province=? and ernings_2013<27593 LIMIT 5 ALLOW FILTERING ;";

	statement = cass_statement_new(query, 1);


	//cass_statement_bind_string(statement, 0, key);

	future = cass_session_execute(this->session, statement);
	cass_future_wait(this->future);

	rc = cass_future_error_code(this->future);
	if (rc != CASS_OK) {
		print_error(this->future);
	} else {
		const CassResult* result = cass_future_get_result(this->future);
		CassIterator* iterator = cass_iterator_from_result(result);

		int i=0;
		while(cass_iterator_next(iterator)) {
			person.push_back(new Person());
			const CassRow* row = cass_iterator_get_row(iterator);
			cass_value_get_int32(cass_row_get_column(row, 0), &person.at(i)->id);
			//cass_value_get_string(cass_row_get_column(row, 1), fname);
			cass_value_get_double(cass_row_get_column(row, 5), &person.at(i)->ernings[0]);
			cass_value_get_double(cass_row_get_column(row, 6), &person.at(i)->ernings[1]);
			cass_value_get_double(cass_row_get_column(row, 7), &person.at(i)->ernings[2]);
			cass_value_get_double(cass_row_get_column(row, 8), &person.at(i)->ernings[3]);
			cass_value_get_double(cass_row_get_column(row, 9), &person.at(i)->ernings[4]);
			i++;
		}

		cass_result_free(result);
		cass_iterator_free(iterator);
	}

	cass_future_free(this->future);
	cass_statement_free(statement);

	return rc;
}

CassError DataBase::select_from_person(const char* query ,DataSet &person) {
	CassError rc = CASS_OK;
	CassStatement* statement = NULL;
	const char* key ="*";
	this->future = NULL;


	statement = cass_statement_new(query, 0);


	//cass_statement_bind_string(statement, 0, key);

	future = cass_session_execute(this->session, statement);
	cass_future_wait(this->future);

	rc = cass_future_error_code(this->future);
	if (rc != CASS_OK) {
		print_error(this->future);
	} else {
		const CassResult* result = cass_future_get_result(this->future);
		CassIterator* iterator = cass_iterator_from_result(result);

		int i=0;
		while(cass_iterator_next(iterator)) {
			person.push_back(new Person());
			const CassRow* row = cass_iterator_get_row(iterator);
			cass_value_get_int32(cass_row_get_column(row, 0), &person.at(i)->id);

			cass_value_get_double(cass_row_get_column(row, 5), &person.at(i)->ernings[0]);
			cass_value_get_double(cass_row_get_column(row, 6), &person.at(i)->ernings[1]);
			cass_value_get_double(cass_row_get_column(row, 7), &person.at(i)->ernings[2]);
			cass_value_get_double(cass_row_get_column(row, 8), &person.at(i)->ernings[3]);
			cass_value_get_double(cass_row_get_column(row, 9), &person.at(i)->ernings[4]);
			i++;
		}
		cass_result_free(result);
		cass_iterator_free(iterator);
	}

	cass_future_free(this->future);
	cass_statement_free(statement);

	return rc;
}

bool DataBase::checkHost(Message& message) {
	return checkHost(message.getLogin(), message.getPassword());
}

bool DataBase::checkHost(const std::string &login, const std::string& password) {
	bool retVal=false;
	CassError rc = CASS_OK;

	CassStatement* statement = NULL;
	this->future = NULL;
	const char* query = "SELECT * FROM mykeyspace.hosts WHERE login=? and password=? LIMIT 1 ALLOW FILTERING ;";
	//std::cout<<"Login and Pass"<<message.getLogin().c_str()<<"\n"<<message.getPassword().c_str()<<std::endl;
	statement = cass_statement_new(query,2);
	cass_statement_bind_string(statement, 0, login.c_str());
	cass_statement_bind_string(statement, 1, password.c_str());

	future = cass_session_execute(this->session, statement);
	cass_future_wait(this->future);

	rc = cass_future_error_code(this->future);
	if (rc != CASS_OK) {
		print_error(this->future);
	}else{
		const CassResult* result = cass_future_get_result(this->future);
		CassIterator* iterator = cass_iterator_from_result(result);

		while(cass_iterator_next(iterator)) {
			retVal=true;
		}
		cass_result_free(result);
		cass_iterator_free(iterator);
	}
	cass_future_free(this->future);
	cass_statement_free(statement);

	return retVal;
}

long DataBase::getDatabaseTime(){
	this->updateDatabaseTimestamp();
	int idx;
#if  defined _WIN32 || defined __CYGWIN__
	int64_t time;
#else
	long time;
#endif
	CassError rc = CASS_OK;
	CassStatement* statement = NULL;
	this->future = NULL;
	const char* query = "SELECT * FROM mykeyspace.time;";

	statement = cass_statement_new(query,0);
	//cass_statement_bind_int32(statement, 0, state);

	future = cass_session_execute(this->session, statement);
	cass_future_wait(this->future);

	rc = cass_future_error_code(this->future);
	if (rc != CASS_OK) {
		print_error(this->future);
	}else{
		const CassResult* result = cass_future_get_result(this->future);
		CassIterator* iterator = cass_iterator_from_result(result);
		
		int i=0;
		while(cass_iterator_next(iterator)) {
			const CassRow* row = cass_iterator_get_row(iterator);
			cass_value_get_int32(cass_row_get_column(row, 0), &idx);
			cass_value_get_int64(cass_row_get_column(row, 1), &(time));
			
		//	//std::cout<<idx<<"	"<<time<<std::endl; globalny czas bazy danych jako epoch
			i++;
		}
		
		cass_result_free(result);
		cass_iterator_free(iterator);
	}
	cass_future_free(this->future);
	cass_statement_free(statement);
	
	return time;
}

bool DataBase::updateDatabaseTimestamp(){
	CassError rc = CASS_OK;
	CassStatement* statement = NULL;
	this->future = NULL;
	const char* query = "UPDATE mykeyspace.time SET time =dateof(now()) WHERE nr=0;";
	statement = cass_statement_new(query, 0);

	this->future = cass_session_execute(this->session,statement);
	cass_future_wait(this->future);

	rc = cass_future_error_code(this->future);
	if (rc != CASS_OK) {
		print_error(this->future);
		return false;
	}
	return true;
}

bool DataBase::getServerList(std::list<Host> &servers){
	CassError rc = CASS_OK;
	bool retVal=false;
	CassStatement* statement = NULL;
	const char* key ="*";
	this->future = NULL;
	const char* query = "SELECT * FROM mykeyspace.servers;";
	statement = cass_statement_new(query, 0);


	//cass_statement_bind_string(statement, 0, key);
	
	future = cass_session_execute(this->session, statement);
	cass_future_wait(this->future);

	rc = cass_future_error_code(this->future);
	if (rc != CASS_OK) {
		print_error(this->future);
	} else {
		const CassResult* result = cass_future_get_result(this->future);
		CassIterator* iterator = cass_iterator_from_result(result);

		int i=0;
		int id, port;
		size_t length;
		const char* ipchar=NULL;
		std::string ip;
		while(cass_iterator_next(iterator)) {
			const CassRow* row = cass_iterator_get_row(iterator);
			cass_value_get_int32(cass_row_get_column(row, 0), &id);
			cass_value_get_string(cass_row_get_column(row, 1), &ipchar, &length);
			cass_value_get_int32(cass_row_get_column(row, 2), &port);
			servers.push_back(Host(std::string(ipchar), port));
		}
		cass_result_free(result);
		cass_iterator_free(iterator);
		retVal=true;
	}

	cass_future_free(this->future);
	cass_statement_free(statement);

	return retVal;

}


bool DataBase::clearServerList(){
	CassError rc = CASS_OK;
	bool retVal=false;
	CassStatement* statement = NULL;
	const char* key ="*";
	this->future = NULL;
	const char* query = "TRUNCATE mykeyspace.servers;";
	statement = cass_statement_new(query, 0);
	
	future = cass_session_execute(this->session, statement);
	cass_future_wait(this->future);

	rc = cass_future_error_code(this->future);
	if (rc != CASS_OK) {
		print_error(this->future);
	} else {
		retVal=true;
	}
		
	cass_future_free(this->future);
	cass_statement_free(statement);

	return retVal;
}

bool DataBase::updateServerList(std::list<Host> servers) {

	
	bool retVal=false;
	clearServerList();
	int i=0;
	for(auto server :servers){
		CassError rc = CASS_OK;
		CassStatement* statement = NULL;
		this->future = NULL;
		const char* query = "INSERT INTO mykeyspace.servers (id,  ip, port) VALUES  (?, ?, ?);";

		statement = cass_statement_new(query, 3);

		cass_statement_bind_int32(statement, 0, i);
		cass_statement_bind_string(statement, 1, server.getAddress().c_str());
		cass_statement_bind_int32(statement, 2, 6666);/*TO DO: Adding server-node comunication port for Host*/
		this->future = cass_session_execute(this->session,statement);
		cass_future_wait(this->future);

		rc = cass_future_error_code(this->future);
		if (rc != CASS_OK) {
			print_error(this->future);
		}else{
			retVal=true;

		}
		cass_future_free(this->future);
		cass_statement_free(statement);
		i++;
	}
		return retVal;
}
