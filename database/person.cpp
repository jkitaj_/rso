#include "person.h"

unsigned int seed_person = 192;
const int e2010 = 3600*12;
const int e2011 = 3700*12;
const int e2012 = 3800*12;
const int e2013 = 3900*12;
const int e2014 = 4000*12;

inline double count(){
	double c;
#if  defined _WIN32 || defined __CYGWIN__
	c = 0.5+1.0*(double)rand()/RAND_MAX;
#else
	c = 0.5+1.0*(double)rand_r(&seed_person)/RAND_MAX;
#endif
	return c;
}

void Person::print(){
	std::cout<<this->id<<" "<<this->fname<<" "<<this->lname<<" "<<this->sex<<" "<<this->province<<" "<<this->ernings[0]<<" "<<this->ernings[1]<<" "<<this->ernings[2]<<" "<<this->ernings[3]<<" "<<this->ernings[4]<<std::endl;

}
Person::Person(){
	this->id=-1;
	this->fname="John";
	this->lname="Doe";
	this->sex="male";
	this->province="-";
	this->ernings[0]=0;
	this->ernings[1]=0;
	this->ernings[2]=0;
	this->ernings[3]=0;
	this->ernings[4]=0;
}

/*
Person::Person(Person &person){
	this->id=person->id;
	this->fname=person->fname_;
	this->lname=person->lname_;
	this->sex=person->sex_;
	this->province=person->province_;
	this->ernings_2010=person->ernings_2010;
	this->ernings_2011=person->ernings_2011;
	this->ernings_2012=person->ernings_2012;
	this->ernings_2013=person->ernings_2013;
	this->ernings_2014=person->ernings_2014;
};*/

Person::Person(cass_int32_t id_, const char* fname_, const char* lname_, const char* sex_, const char* province_){ 
	this->id=id_;
	this->fname=fname_;
	this->lname=lname_;
	this->sex=sex_;
	this->province=province_;
	this->ernings[0]=(int)e2010*count();
	this->ernings[1]=(int)e2011*count();
	this->ernings[2]=(int)e2012*count();
	this->ernings[3]=(int)e2013*count();
	this->ernings[4]=(int)e2014*count();
	};